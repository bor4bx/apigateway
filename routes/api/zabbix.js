const { Router } = require('express');
const express = require('express');
const zrouter = express.Router();
const mysql = require("mysql");
const zabbix = require('./zabbix');
const axios = require('axios');

const {
    globalGatewayCache
} = require("../../configs/cache.js");


const {
    zqueries,
    zmConfig,
    zabbixLogin

} = require("../../configs/zabbix");



zrouter.get("/sql/:sql", async (req, res) => {

    if(globalGatewayCache.has('zabbix_'+req.params.sql)){
        console.log("Returning Zabbix " + 'zabbix_'+req.params.sql  + " from Cache");
        res.json(globalGatewayCache.get('zabbix_'+req.params.sql));
    }
    /* #swagger.description = 'Zabbix  urilizing back end sql queries instead of Zabbix REST calls: <br>
        &emsp;<b>full:</b> Zabbix SKU search results <br>
        &emsp;<b>condenced:</b> Sku Condenced <br>
        &emsp;<b>probManagement:</b> Top Problems from the last 30 days <br>
        &emsp;<b>problems:</b> Problems in Zabbix  <br>
        &emsp;<b>problemsCount:</b> Count of problems grouped by severity <br>
    '

    #swagger.produces = ["application/json"]
    #swagger.responses[200] = {
        description: 'Zabbix  urilizing back end sql queries instead of Zabbix REST calls',
        schema:{
            anyOf: [
                {
                    $ref: "#/definitions/full",
                },
                {
                    $ref: "#/definitions/condenced"
                },
                {
                    $ref: "#/definitions/problemManagment"
                },
                {
                    $ref: "#/definitions/problems"
                },
                {
                    $ref: "#/definitions/problemsCount"
                },
            ]
        }
    } 
    */

    else {
        const pool = mysql.createPool(zmConfig);
        console.log("Not Returning Zabbix " + 'zabbix_'+req.params.sql  + " from Cache");
        if (zqueries[req.params.sql]) {
            var companyID = {};
            var getCompanyIDCache = {};
            pool.query(zqueries[req.params.sql], (error, results) => {
                if (!results) {
                    res.json({ status: error });
                } else {
                    // if(req.params.sql == 'full'){
                    //     let count = 0;
                    //     if(globalGatewayCache.has("device2Company") && count < 3){
                    //         companyID = globalGatewayCache.get("device2Company");                          

                    //     }
                    //     while(!globalGatewayCache.has("manage_ID2RecID")){
                    //                                const cacheSet = {
                    //             method: 'get',
                    //             url: 'http://10.100.10.150:5000/cwapi/queries/cwCompany'
                    //         }
                    //         //let caching = 
                    //         axios(cacheSet);
                    //         count++
                          
                    //     }
                    //     getCompanyIDCache = globalGatewayCache.get("manage_ID2RecID")
                    //     for(i=0; i < results.length; i++){
                            
                    //         if(results[i]["sku"] == 'MS - Windows Server' || 
                    //            results[i]["sku"] == 'MS - Unix/Linux Server' || 
                    //            results[i]["sku"] == 'MS - Citrix Cloud Infra' || 
                    //            results[i]["sku"] ==  'MS - Citrix Infra' || 
                    //            results[i]["sku"] == 'MS - Citrix Master HSD' || 
                    //            results[i]["sku"] == 'MS - Citrix Hybrid Infra')
                    //         {
                    //                 companyID[results[i]['ServerName']] = {'company_ID': results[i]['Company_ID'], 
                    //                                                         'company_RecID': getCompanyIDCache[results[i]['Company_ID']]['company_RecID'], 
                    //                                                         'company_Name': getCompanyIDCache[results[i]['Company_ID']]['company_Name']}
                    //         }
                    //     }
                    //     globalGatewayCache.set("device2Company", companyID);
                    // } 
                    // globalGatewayCache.set("zabbix_"+req.params.sql, results);
                    // res.json(globalGatewayCache.get("zabbix_"+req.params.sql))    
                    res.json(results);                   
                }
            })
        } else res.send('no params *' + req.params.sql + '*');
    }
}); 


zrouter.get("/sqltable/:sql", async (req, res) => {


    /*
        #swagger.description = "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table: <br>
        &emsp;<b>full:</b> Zabbix SKU search results <br>
        &emsp;<b>condenced:</b> Sku Condenced <br>
        &emsp;<b>probManagement:</b> Top Problems from the last 30 days <br>
        &emsp;<b>problems:</b> Problems in Zabbix  <br>
        &emsp;<b>problemsCount:</b> Count of problems grouped by severity <br>
    "
        #swagger.produces = ["text/html"]
        #swagger.responses[200] = {
            description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
            schema: {
                oneOf:[
                    {
                        $ref: "#/definitions/fullTable",
                    },
                    {
                        $ref: "#/definitions/condencedTable",
                    }
                ]
            }
         } 
    */
    const flatten = require('flat').flatten;
    const row = html => `<tr>\n${html}</tr>\n`,
        heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
        datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));
    const pool = mysql.createPool(zmConfig);

    if (zqueries[req.params.sql]) {
        pool.query(zqueries[req.params.sql], (error, results) => {
            if (!results) {
                res.json({ status: "Not found!" });
            } else {
                //res.json(results);
                res.send(`<table border="1">
         ${heading(results[0])}
          ${results.reduce((html, object) => (html + datarow(flatten(object))), '')}
        </table>`)
            }
        })
    } else res.send('no params *' + req.params.sql + '*');

});

zrouter.get('/apikey', async (req, res) => {
    // #swagger.ignore = true
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
    zabbixLogin().then(async function (response) {
        // handle success
        let apikey = response;
        res.send(apikey.data.result);
    });


});

// zrouter.get('/problems', async (req, res) => {
//     zabbixLogin().then(async function (response) {
//         let apikey = response.data.result
//         //async function zabbixProblems() {
//         //    try {
//         const config = {
//             jsonrpc: '2.0',
//             method: 'problem.get',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Accept': '*/*',
//                 'Host': 'zmonitoring.choicecloud.com',
//                 'Accept-Encoding': 'gzip, deflate, br',
//                 'Connection': 'keep-alive',
//             },
//             url: 'https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php',


//             data: {

//                 "jsonrpc": "2.0",
//                 "method": "problem.get",
//                 "id": 1,
//                 "params": {
//                     //                   output: 'extend',
//                     "output": [
//                         'eventid',
//                         'objectid',
//                         'clock',
//                         'ns',
//                         'name',
//                         'acknowledged',
//                         'priority',
//                         'severity'
//                     ],
//                     filter: {
//                         severity: (5),
//                         recent: 0,
//                         value: 1
//                     },
//                     //sortfield: "priority",
//                     sortorder: 'DESC',
//                     limit: 100,
//                     recent: false,
//                     selectSuppressionData: "extend",
//                     countOutput: true



//                 },
//                 auth: apikey,
//                 id: 1

//             },



//         }
//         console.log(apikey);
//         axios(config).then(function (response) { res.json(response.data); console.log(response); }).catch(function (error) { console.log(error); })

//     })

// })

// zrouter.get('/event-client', async (req, res) => {
//     const flatten = require('flat').flatten;
//     const row = html => `<tr>\n${html}</tr>\n`,
//         heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
//         datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));

//     const { ZabbixClient } = require("zabbix-client")
//     const client1 = new ZabbixClient("https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php")
//     const api = await client1.login("nocboard", "timeout")

//     const problems = await api.method("event.get")
//         .call({
//             source: 0,
//             object: 0,
//             output: "extend",
//             filter: {
//                 severity: (
//                     5
//                 ),
//                 r_eventid: 0,

//             },
//             //sortfield: "priority",
//             selectSuppressionData: 'extend',
//             sortorder: 'ASC',
//             limit: 10,
//             recent: "true",

//         })

//         .then(result => {
//             //   res.json(result)
//             res.send(`<table>
//          ${heading(result[0])}
//           ${result.reduce((html, object) => (html + datarow(flatten(object))), '')}
//         </table>`)
//         })
//         .catch(err => {
//             // err instanceof ZabbixResponseException; // true
//             console.log("this error", err.message); // Incorrect method "thismethod.doesnotexist"
//         })

// })


module.exports = zrouter