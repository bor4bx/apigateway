const { Router } = require('express');
const express = require('express');
const cwrouter = express.Router();
const mssql = require("mssql");
//const ConnectWiseRest = require('connectwise-rest');
const { ManageAPI } = require('connectwise-rest');
const RequestIp = require('@supercharge/request-ip')

const cw = new ManageAPI({    
    companyId: process.env.CWM_companyId,
    publicKey: process.env.CWM_publicKey,
    privateKey: process.env.CWM_privateKey,
    companyUrl: process.env.CWM_companyURL,
    clientId: process.env.CWM_clientId,
    timeout: 20000,             // optional, request connection timeout in ms, defaults to 20000
    retry: false,               // optional, defaults to false
    retryOptions: {             // optional, override retry behavior, defaults as shown
        retries: 4,               // maximum number of retries
        minTimeout: 50,           // number of ms to wait between retries
        maxTimeout: 20000,        // maximum number of ms between retries
        randomize: true,          // randomize timeouts
    },
    debug: false,               // optional, enable debug logging
    logger: (level, text, meta) => { } // optional, pass in logging function
});

const {
    cwMConfig,
    queries
} = require("../../configs/cwManage1.js");

const {
    globalGatewayCache
} = require("../../configs/cache.js");

//and status/name not in ('Cancelled', 'Completed', 'Resolved' , 'Closed', 'Reviewed','Backup Remediation','--> Escalate to PS','Child Ticket','Waiting on Client','Change Request Submitted')
// and recordType='ServiceTicket'

// and priority/name != 'No SLA'
// and priority/name like 'MS*'        
// and summary != 'Quarterly Backup Test (Unitrends)'
// and status/name != 199
// and board/name in ('Automation',,'Managed Security','Citrix','Network','NOC','Patching','Security','Virtualization and Storage','Backup')
        
const tcktcount = {
    sla: `board/name in ('Automation',,'Managed Security','Citrix','Network','NOC','Patching', 'Internal MS', 'Security','Virtualization and Storage','Backup') 
    and status/name not in ('Cancelled', 'Complete Pending', 'Completed', 'Resolved' , 'Closed', 'Reviewed','Backup Remediation','--> Escalate to PS','Child Ticket','Waiting on Client','Change Request Submitted')
    and isInSla = false 
    and priority/name != 'No SLA' 
    and slaStatus like 'Reso*'
    and priority/name like 'MS*'
    `,
    productionDown: `closedFlag = false
        and status/name not in ('Cancelled', 'Completed', 'Resolved' ,'Reviewed','Backup Remediation','--> Escalate to PS','Child Ticket','Waiting on Client')
        and recordType='ServiceTicket'
        and isInSla=false
        and priority/name = 'MS 1 - Production Down'
        and summary != 'Quarterly Backup Test (Unitrends)'
        and board/name in ('Automation','Citrix','Network','NOC','Patching','Security','Virtualization and Storage','Backup')`

}
/**
 * This GET request has a switch statement that deterines what SQL statement it will run. Each SQL statement has different
 * responses therfore, the below defines the GET request as if it is multple individual requests. This will allow swagger
 * to display different directions for each.
 * 
 * @openapi
 * /cwmanage/cwHash
 *   get:
 *     description: Connectwise Ticket Subject Count
 *     tags: [ticket_count]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: tickets
 *         schema:
 *           type: array
 *           $ref: '#/definitions/cwHash'
 * /cwmanage/SLA
 *   get:
 *     description: Connectwise Tickets that are past Resolve Date
 *     tags: [SLA]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: tickets
 *         schema:
 *           type: array
 *           $ref: '#/definitions/SLA'
 * /cwmanage/SLACount
 *  get:
 *    description: Connectwise Tickets that are past Resolve Date
 *    tags: [SLACount]
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: tickets
 *        schema:
 *          type: array
 *          $ref: '#/definitions/SLACount'
 *
 * /queries/:sql
 *  get:
 *    description: Connectwise urilizing back end sql queries instead of Connectwise REST calls
 *    tags: [SLACount]
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: tickets
 *        schema:
 *          type: array
 *          $ref: '#/definitions/SLACount'
 */



cwrouter.get("/queries/:sql", async (req, res) => {
    console.log(req.params.sql);
    if(globalGatewayCache.has('manage_'+req.params.sql)){
        console.log('Returning '+ 'manage_'+req.params.sql+ ' from Cache');
        res.send(globalGatewayCache.get('manage_'+req.params.sql));
    }else{
        console.log('NOT Returning '+ 'manage_'+req.params.sql+ ' from Cache');
     /* #swagger.description = 'Connectwise urilizing back end sql queries instead of Connectwise REST calls<br>
       &emsp;<b>cwHash:</b> Top Tickets for the past month <br>
       &emsp;<b>cwSLA:</b> Tickets that have violated SLA <br>
       &emsp;<b>cwSLACount:</b> Count of tickets that have violated SLA <br>
       &emsp;<b>cwDailyBackup:</b> Daily Onsite Backup Checks <br>
       &emsp;<b>cwDailyBackupOffsite:</b> Daily Offsite Backup Checks <br>
       &emsp;<b>cwSku:</b> Connectwise Sku list <br>
       &emsp;<b>cwTime:</b>  <br>
       '

       #swagger.produces = ["application/json"]
        #swagger.responses[200] = {
            description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
            schema: {
                oneOf:[
                    {
                        $ref: "#/definitions/cwHash",
                    },
                    {
                        $ref: "#/definitions/cwSLA",
                    },
                    {
                        $ref: "#/definitions/cwSLACount",
                    },
                    {
                        $ref: "#/definitions/cwDailyBackup",
                    },
                    {
                        $ref: "#/definitions/cwDailyBackupOffsite",
                    },
                    {
                        $ref: "#/definitions/cwSku",
                    }
                ]
            }
         } 
    */
     
        mssql.connect(cwMConfig, function (err) {
            if (err)
                console.log(err);
console.log(req.params.sql);
            var pool = new mssql.Request();
            if (queries[req.params.sql]) {
                let result = pool.query(queries[req.params.sql], function (err, recordset) {
                    if (err)
                        console.log(err);

                    if(req.params.sql = 'cwCompany')
                    {
                        let temp = {};
                        let companyRec = {};
                        let companyID = {};
                        let companyName = {};

                    for(let i=0; i<recordset.recordsets[0].length;i++)
                    { 
                        let id = recordset.recordsets[0][i]['Company_ID'];
                        let rec = recordset.recordsets[0][i]['Company_RecID'];
                        let name = recordset.recordsets[0][i]['Company_Name'];
                        companyID[id] = {'Company_RecID': rec, 'Company_Name': name};
                        companyRec[rec] = {'Company_ID': id, 'Company_Name': name};
                        companyName[name] = {'Company_RecID': rec, 'Company_ID': id};
                        

                        if(typeof recordset.recordsets[0][i].Userfield_7 !== 'undefined' && recordset.recordsets[0][i].Userfield_7 && recordset.recordsets[0][i].Company_RecID !== 'undefined'){
                            temp[recordset.recordsets[0][i].Userfield_7] = recordset.recordsets[0][i].Company_RecID;

                            
                        }
                    }
                    
                    globalGatewayCache.set("manage_Huntress", temp);
                    globalGatewayCache.set("manage_ID2RecID", companyID);
                    globalGatewayCache.set("manage_RecID2ID", companyRec);
                    globalGatewayCache.set("manage_Name2RecandID", companyName);
                    //console.log(globalGatewayCache.get("manage_Huntress"));
                    }
                    globalGatewayCache.set('manage_'+req.params.sql, recordset.recordsets[0]);
                    res.send(globalGatewayCache.get('manage_'+req.params.sql));
                });
            } else
                res.send('no params *' + req.params.sql + '*');


        })
    }
})
cwrouter.get("/queriesTable/:sql", async (req, res) => {
    /* #swagger.description = 'Connectwise urilizing back end sql queries instead of Connectwise REST calls and detlivers an HTML table instead of Json Object<br>
      &emsp;<b>cwHash:</b> Top Tickets for the past month <br>
      &emsp;<b>cwSLA:</b> Tickets that have violated SLA <br>
      &emsp;<b>cwSLACount:</b> Count of tickets that have violated SLA <br>
      &emsp;<b>cwDailyBackup:</b> Daily Onsite Backup Checks <br>
      &emsp;<b>cwDailyBackupOffsite:</b> Daily Offsite Backup Checks <br>
      &emsp;<b>cwSku:</b> Connectwise Sku list <br>
      &emsp;<b>cwTime:</b>  <br>
      '

      #swagger.produces = ["text/html"]
       #swagger.responses[200] = {
           description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
           schema: {
               oneOf:[
                   {
                       $ref: "#/definitions/cwHash",
                   },
                   {
                       $ref: "#/definitions/cwSLA",
                   },
                   {
                       $ref: "#/definitions/cwSLACount",
                   },
                   {
                       $ref: "#/definitions/cwDailyBackup",
                   },
                   {
                       $ref: "#/definitions/cwDailyBackupOffsite",
                   },
                   {
                       $ref: "#/definitions/cwSku",
                   }
               ]
           }
        } 
   */
    const row = html => `<tr>\n${html}</tr>\n`,
        heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
        datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));

    const flatten = require('flat').flatten;

    mssql.connect(cwMConfig, function (err) {
        if (err)
            console.log(err);

        var pool = new mssql.Request();

        if (queries[req.params.sql]) {
            let result = pool.query(queries[req.params.sql], function (err, recordset) {
                if (err)
                    console.log(err);


                res.send(`<table>
                ${heading(recordset.recordset[1])}
                
                 ${recordset.recordsets[0]?.reduce((html, object) => (html + datarow(flatten(object))), '')}
               </table>`)
            });
        } else
            res.send('no params *' + req.params.sql + '*');


    })
})

cwrouter.get("/queriesTableDates/:sql/:month/:year", async (req, res) => {
    const row = html => `<tr>\n${html}</tr>\n`,
        heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
        datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));

    const flatten = require('flat').flatten;
    var month = req.params.month;
    var year = req.params.year;
    var lday = 28;
    var hash = `SELECT TOP 30
        summary,
        count( HASHBYTES('md5', Summary)) as hash,
        Max(Date_Entered_UTC) as 'Last Occur'
        FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
        where 
        Billing_unit_desc like '%Man%'
        and
        Board_Name not in ('MS Presales', 'Patching', 'Internal MS')
        and
        Date_Entered_UTC > '${year}-${month}-1' and Date_Entered_UTC < '${year}-${month}-${lday}'
        and 
        summary not like 'CS - ScreenConnect - Disc%'
        and
        summary not like '%BITS%'
        and 
        summary not like '%Backup%'
        group by HASHBYTES('md5', Summary),summary
        order by hash desc`;

    mssql.connect(cwMConfig, function (err) {
        if (err)
            console.log(err);

        var pool = new mssql.Request();

        if (queries[req.params.sql]) {
            let result = pool.query(queries[req.params.sql], function (err, recordset) {
                if (err)
                    console.log(err);
                    


                res.send(`<table>
                ${heading(recordset.recordset[1])}
                
                 ${recordset.recordsets[0]?.reduce((html, object) => (html + datarow(flatten(object))), '')}
               </table>`)
            });
        } else
            res.send('no params *' + req.params.sql + '*');


    })
})

cwrouter.route('/ticket')
    .post(async (req, res) => {
        /* #swagger.description = 'ConnectwisCreate Ticket Creation'
   
          #swagger.produces = ["application/json"]
           #swagger.responses[200] = {
               description: "Returns full details of the ticket",
               schema: {
                           $ref: "#/definitions/newTicketPost",
                       },
               
            }
         
       */
        
        // res.send('hello ' + req.body.Company) BetaTest
        console.log(RequestIp.getClientIp(req));
        cw.ServiceAPI.postServiceTickets({
        //cw.ServiceDeskAPI.Tickets.createTicket({
            summary: req.body.summary || 'test',
            board: {
                name: "Automation"
            },
    	    priority: {
	    	id: req.body.priority
	    },

            Company: {
                identifier: req.body.Company
            },
            Status: {
                name: 'Unassigned'
            },
            initialDescription: req.body.initialDescription,
            recordType: "ServiceTicket",
        })
            .then((ticket) => {
                console.log(ticket['id']);
                res.json(ticket)
            })
            .catch((err) => { 
                console.log(err); 
                let errorstatus = { 
                    'BadRequest': 400,
                    'Unauthorized': 401,
                    'PaymentRequired': 402,
                    'Forbidden': 403,
                    'NotFound': 404,
                    'MethodNotAllowed': 405,
                    'NotAcceptable': 406,
                    'ProxyAuthenticationRequired': 407,
                    'RequestTimeout': 408,
                    'Conflict': 409,
                    'Gone': 410          
            
            }
            let status = 400;
            //console.log(err.errors['code']);
            
            //if(typeof errorstatus[err.errors['code']] != 'undefined'){ let status = errorstatus[err.errors['code']] }
                
            res.status(status).send(err);
            console.log('Did not Create a Ticket for ' + req.body.Company)
            
            })
   
    })


cwrouter.route('/boardStatus/:id')
    .get(async (req, res) => {
        params = {
            fields: 'id,name',
        }
        // res.send('hello world')
       // cw.ServiceDeskAPI.getServiceBoardsByID(req.params.id, params)
        cw.ServiceAPI.getServiceBoardsByID(req.params.ticketnum)
        .then((result) => { res.json(result)})
        .catch((err) => {res.send(err)})
    })

cwrouter.route('/ticket/?:ticketnum')
    .get(async (req, res) => {
        /* #swagger.description = 'ConnectwisCreate Get Ticket by ticket number'
   
          #swagger.produces = ["application/json"]
           #swagger.responses[200] = {
               description: "Returns full details of the ticket",
               schema: {
                           $ref: "#/definitions/newTicketPost",
                       },
               
            }
         
       */
        console.log('Your IP: ' + RequestIp.getClientIp(req));

        cw.ServiceAPI.getServiceTicketsById(req.params.ticketnum)
            .then(function (result) {
                res.json(result);
            })
            .catch(function (error) {
                console.log(error + ' Ticket Num: ' + req.params.ticketnum);
            });
    }
    )
    .put(async (req, res) =>{
        /* #swagger.description = 'ConnectwisCreate Ticket Update'
   
          #swagger.produces = ["application/json"]
           #swagger.responses[200] = {
               description: "Returns full details of the ticket",
               schema: {
                           $ref: "#/definitions/newTicketPost",
                       },
               
            }
         
       */
        cw.ServiceAPI.putServiceTicketsById(req.params.ticketnum, [{
            op: 'replace',
            path: 'status',
            value: { 'name': 'Cancelled' }
        }])
        .then((result) => { 
            res.json(result)
        
        })
        .catch((err) =>{
            console.log(err)

        })
    })
    

// /service/tickets/{parentId}/notes
cwrouter.route('/ticketNotes/:ticketnum') //cwTicketNotes
    .post(async function (req, res) {
        /* #swagger.description = 'Connectwise Update Ticket Notes'
   
          #swagger.produces = ["application/json"]
           #swagger.responses[200] = {
               description: "Returns full details of the ticket",
               schema: {
                           $ref: "#/definitions/cwTicketNotes",
                       },
               
            }
        */
        // let notes = 'This is a test note 2'
        console.log("body of message:" + req.body.note);
        let note = {
            "id": 0,
            "ticketId": req.params.ticketnum,
            "text": req.body.note,
            "detailDescriptionFlag": false,
            "internalAnalysisFlag": true,
            "resolutionFlag": false,
            "issueFlag": false,
            // "member": {
            //     "id": 423,
            //     "identifier": "LCatherall",
            //     "name": "Liam Catherall",
            //  },
            // "dateCreated": "2021-10-14T23:25:47Z",
            // "createdBy": "LCatherall",
            "customerUpdatedFlag": false,
            "processNotifications": true,
            "internalFlag": true,
            "externalFlag": false,
        }
       // cw.ServiceDeskAPI.ServiceNotes.createServiceNote(req.params.ticketnum, note)
        cw.ServiceAPI.postServiceTicketsByParentIdNotes(req.params.ticketnum, note)
            .then(function (result) {
                res.json(result);
            })
            .catch(function (error) {
                console.log(error);
            });
        })
    .get(async function (req, res) {
        /* #swagger.description = 'Connectwise Get Ticket Notes'
   
          #swagger.produces = ["application/json"]
           #swagger.responses[200] = {
               description: "Returns full details of the ticket",
               schema: {
                           $ref: "#/definitions/cwTicketNotes",
                       },
               
            }
        */
         
        let param = {
            "orderBy": "id desc"
        }
        //cw.ServiceDeskAPI.ServiceNotes.getServiceNotes(req.params.ticketnum, param)
        cw.ServiceAPI.getServiceTicketsByParentIdNotes(req.params.ticketnum, param)
            .then(function (result) {
                res.json(result);
            })
    })



cwrouter.route('/tickets/:query')
    .get(async (req, res) => {

        const fields = "id,summary,board/name,board/id,owner/name,status/name,company/identifier,priority/name,source/name,dateEntered,isInSla,_info/updatedBy"
        const c = tcktcount[req.params.query]//`closedFlag = false AND board/name='NOC'`
//        const c = `closedFlag = false AND board/name='NOC'`

        const params = {
        // conditions: tcktcount[req.params.query],
            conditions:  c,
            fields: fields,
            orderby: 'id',
            pageSize: 500,
        //    page: 1

        }

       //cw.ServiceDeskAPI.Tickets.getTickets(params)
       cw.ServiceAPI.getServiceTickets(params)
            .then(function (result) {
                res.json(result);
            })
            .catch(function (error) {
                console.log('This is the error', error);
            });
    })

    cwrouter.route('/tickets2/:query')
    .get(async (req, res) => {

        //const fields = "id,summary,board/name,board/id,owner/name,status/name,company/identifier,priority/name,source/name,dateEntered,isInSla,_info/updatedBy"
        const c = tcktcount[req.params.query]//`closedFlag = false AND board/name='NOC'`
//        const c = `closedFlag = false AND board/name='NOC'`

        const params = {
         conditions: `board/name in ('Automation',,'Managed Security','Citrix','Network','NOC','Patching','Security','Virtualization and Storage','Backup') 
         and status/name not in ('Cancelled', 'Completed', 'Resolved' , 'Closed', 'Reviewed','Backup Remediation','--> Escalate to PS','Child Ticket','Waiting on Client','Change Request Submitted')
         and isInSla = false 
         and priority/name != 'No SLA' 
         and slaStatus like 'Reso*'
         and priority/name like 'MS*'
         `,
        //    conditions:  c,
           // fields: fields,
            orderby: 'id',
            pageSize: 500,
        //    page: 1

        }

       //cw.ServiceDeskAPI.Tickets.getTickets(params)
       cw.ServiceAPI.getServiceTickets(params)
            .then(function (result) {
                res.json(result);
            })
            .catch(function (error) {
                console.log('This is the error', error);
            });
    })

cwrouter.get('/ticketCount/:query', async (req, res) => {

    const fields = "id"
    const params = {
        conditions: tcktcount[req.params.query],
        fields: fields,
        orderby: 'id',
    }

    cw.ServiceAPI.getServiceTicketsCount(params)
        .then(function (result) {
            res.json(result);
        })
        .catch(function (error) {
            console.log(error);
        });
})

cwrouter.route('/childTicket/:parentID', async (req, res) => {
    for(var i=0; i < req.body.childeren.length; i++){

        const params = {
            parentID: req.params.parentID,
            bundle: req.body.childeren[i]
        }
        cw.ServiceAPI.postServiceTicketsByParentIdAttachChildren(params)
    }

})


module.exports = cwrouter;
