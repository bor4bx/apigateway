//   import * as xdr from '@paloaltonetworks/pan-cortex-xdr'  assert { type: 'module' }
// const { XdrApi, createXdrApi } = require('@paloaltonetworks/pan-cortex-xdr');
const { Router } = require('express');
const express = require('express');
const huntressouter = express.Router();
const axios = require('axios');

const huntressBaseFqdn = 'https://api.huntress.io/v1/'
const huntressorgs = 'organizations?limit=400'
const huntressincident = 'incident_reports?limit=400&status=sent'
const auth = 'aGtfNmIzYTBhYTc1NjdmMDFjYWM1OWU6aHNfYjFkMWM1MWNlNjdhNDZhYTY5Y2EwY2MyOTk0NzJmZTI='



const {
    globalGatewayCache
} = require("../../configs/cache.js");

//console.log(globalGatewayCache.getStats())

huntressouter.route("/orgs") 
   .get(async (req, res) => {
       /* #swagger.description = 'Huntress Orgs'
 
        #swagger.produces = ["application/json"]
        #swagger.responses[200] = {
            description: "Returns All Choice Managed Orgs and their Endpoints, Agent Count and Incident Count",
            schema: {
                        $ref: "#/definitions/huntorgs",
                    },
            
             }
 
    */

    const getorgs = {
        method: 'get',
        url: huntressBaseFqdn+huntressorgs,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            'Authorization': 'Basic '+auth
        },

    }
   const response = await axios(getorgs);
    count = 0
    while(!globalGatewayCache.has("manage_Huntress") && count < 3){
        const cacheSet = {
            method: 'get',
            url: 'http://10.100.10.152:5000/cwapi/queries/cwCompany'
        }
        let caching = await axios(cacheSet);
        caching = '';
       count++;
   }
   if(count > 1){res.send('error getting Manage Huntress Cache Link')}
    else
    {
        console.log('******** Manage Huntress Cache is good go go ************ or  Count = ' + count);
        console.log(globalGatewayCache.getStats());
        let link = globalGatewayCache.get("manage_Huntress");
        for(var i in response.data.organizations){
            response.data.organizations[i]['sku'] = 'SECaaS - EPPAAS - MDR';
            let id =  response.data.organizations[i].id;
            if(link[id] !== 'undefined' && link[id]){
                response.data.organizations[i]['Company_RecID'] = link[id];
            }else{response.data.organizations[i]['Company_RecID'] = null}

        }
        res.send(response.data.organizations);
    }

})
huntressouter.route("/incidents") 
    .get(async (req, res) => {
        /* #swagger.description = 'Huntress Incidents'

        #swagger.produces = ["application/json"]
        #swagger.responses[200] = {
            description: "Returns All  Huntress Incidents",
            schema: {
                        $ref: "#/definitions/huntincidents",
                    },
            
            }

    */
    const getincidnet = {
        method: 'get',
        url: huntressBaseFqdn+huntressincident,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            'Authorization': 'Basic '+auth
        },

    }
    const response = await axios(getincidnet);

    res.send(response.data.incident_reports);

})

module.exports = huntressouter;
