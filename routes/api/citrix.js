const { Router } = require('express');
const express = require('express');
const router = express.Router();
const citrix = require('./citrix');
const axios = require('axios');

const {
    getBaerer,citrixCloudLic
} = require("../../configs/citrixcloud.js");



router.get('/lic/:tenant', async (req, res) => {
    /* #swagger.description = 'Citrix Cloud Tenant Lic Count<br>
       &emsp;<b>singleTenantsUsage:</b> Singe Tenant Lookup<br>
       &emsp;<b>multiTenantsUsage:</b> Multi Tenant Lookup<br>'
 
      #swagger.produces = ["application/json"]
       #swagger.responses[200] = {
           description: "Returns Licences for tenant",
           schema: {
                    
                        
                        "customerId": "be4258lg1b38",
                        "orgId": "52279586",
                        "displayName": "ChoiceSolutions",
                        "editionName": "Virtual Apps and Desktops Service",
                        "totalCommitCount": 2,
                        "totalUsageCount": 40,
                        "totalOverageCount": 38,
                        "totalUsagePercent": 2000
                        
                    
                   },
           
        }
    */

    let lic = await citrixCloudLic();
    //res.send(lic);
     
    switch (req.params.tenant){

            case "multi":
                res.send(lic.multiTenantsUsage);
                break;
            default:

                res.send(lic.singleTenantsUsage);
    }

})

module.exports = router;