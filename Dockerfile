FROM keymetrics/pm2:latest-alpine
WORKDIR app

# Bundle APP files
COPY .  /app
COPY package.json .
COPY index.js .

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

# Expose the listening port of your app
EXPOSE 5001

# Show current folder structure in logs
RUN ls -al -R
CMD [ "pm2-runtime", "start", "index.js" ]
