const swaggerAutogen = require('swagger-autogen')();

const doc = {
    info: {
        version: '1.0.0',      // by default: '1.0.0'
        title: 'Choice Rest API Gateway',        // by default: 'REST API'
        description: 'Choice Rest API Gateway',  // by default: ''
    },
    host: '10.100.10.150:5000',      // by default: 'localhost:3000'
    servers: '10.100.10.150:5000',
    basePath: '',  // by default: '/'
    schemes: ['http'],   // by default: ['http']
    consumes: ['application/json'],  // by default: ['application/json']
    produces: ['application/json','text/html'],  // by default: ['application/json']

          // by default: empty object (OpenAPI 3.x)
    definitions: {
        huntincidents: {
                  "id": 423423,
                  "status": "sent",
                  "summary": null,
                  "body": "Huntress detected the following Potentially Unwanted Program (PUP)...",
                  "updated_at": "2022-06-28T19:08:31Z",
                  "agent_id": 2432423,
                  "status_updated_at": "2022-06-28T19:08:31Z",
                  "organization_id": 324324,
                  "sent_at": "2022-06-28T19:08:31Z",
                  "account_id": 6948,
                  "subject": "LOW - Incident on ptmw007678 (PTMW, Inc.)",
                  "remediations": [
                    {
                      "id": 432423,
                      "type": "service",
                      "status": "unapproved",
                      "details": {
                        "name": "SlimService"
                      },
                      "completable_by_task_response": true,
                      "completable_manually": false,
                      "display_action": "Delete Service",
                      "approved_at": null,
                      "approved_by": null,
                      "completed_at": null
                    }      
                  ],
                  "footholds": "Foothold 1 - https://choicesolutions.huntress.io/org/432423/autoruns/432432...",
                  "severity": "low",
                  "closed_at": null,
                  "indicator_types": [
                    "footholds"
                  ],
                  "indicator_counts": {
                    "footholds": 2,
                    "monitored_files": 0,
                    "process_detections": 0,
                    "ransomware_canaries": 0,
                    "antivirus_detections": 0
                  }      
        },
        huntorgs:{
                "id": 98393,
                "name": "Test",
                "created_at": "2021-07-30T22:17:06Z",
                "updated_at": "2022-01-20T19:30:06Z",
                "account_id": 6948,
                "key": "test",
                "notify_emails": [
                  "cwatson@choicesolutions.com"
                ],
                "incident_reports_count": 0,
                "agents_count": 0
        },
        fullTable: {
           html: "<html><body><tr><td>Company_ID</td><td>qty</td><td>ServerNmae</td><td>clock</td><td>ItemName</td><td>sku</td><td>key_</td></tr></body><html>"  
        },
        condencedTable: {
            html: "<html><body><tr><td>Company_ID</td><td>CC - Tier 1  Total Storag</td><td>CC - Tier 1 Storage</td><td>CC - vCPU</td><td>CC - vMemory</td><td>CC - Tier 2 Storage</td><td>Unitrends Backup Appliances</td></tr></body><html>"
        },
        condenced: {
            "Company_ID": "CWMCustomer ID",
            "CC - Tier 1  Total Storage": 4956.35,
            "CC - Tier 1 Storage": 1103.74,
            "CC - vCPU": 32,
            "CC - vMemory": 96,
            "CC - Tier 2 Storage": 1461.64,
            "MS - Windows Server": 3,
            "Unitrends Backup Appliances": 3,
            "MS - Hypervisor": null,
            "MS - Citrix Master HSD": null,
            "MS - Citrix Master VDI": null,
            "MS - Citrix Infra": null,
            "MS - Citrix Hybrid Infra": null,
            "MS - Citrix Cloud Infra": null
        },
        endpoints: {
            "endpoint_id": "da0cb518dbb7465caa2d74511ae288e1",
            "endpoint_name": "DFWACECC01",
            "endpoint_type": "AGENT_TYPE_SERVER",
            "endpoint_status": "CONNECTED",
            "os_type": "AGENT_OS_WINDOWS",
            "os_version": "10.0.17763",
            "ip": [
                "10.200.215.151"
            ],
            "users": [
                "ChoiceCloud.com\\eric.bubba"
            ],
            "domain": "ChoiceCloud.com",
            "alias": "",
            "first_seen": 1616420169627,
            "last_seen": 1646058101195,
            "content_version": "400-84384",
            "installation_package": "Windowx64v506",
            "active_directory": null,
            "install_date": 1616420169632,
            "endpoint_version": "7.2.3.19377",
            "is_isolated": "AGENT_UNISOLATED",
            "isolated_date": null,
            "group_name": [
                "ACEAuditingComplianceEduc"
            ],
            "operational_status": "PROTECTED",
            "operational_status_description": "[]",
            "scan_status": "SCAN_STATUS_NONE",
            "content_release_timestamp": 1645746031000,
            "last_content_update_time": 1645749944905
        },
        incidents: {
            "incident_id": "32410",
            "incident_name": null,
            "creation_time": 1645806098428,
            "modification_time": 1646074418323,
            "detection_time": null,
            "status": "new",
            "severity": "medium",
            "description": "24 'WildFire Malware' alerts prevented by XDR Agent on 2 hosts  involving 2 users",
            "assigned_user_mail": null,
            "assigned_user_pretty_name": null,
            "alert_count": 24,
            "low_severity_alert_count": 0,
            "med_severity_alert_count": 24,
            "high_severity_alert_count": 0,
            "user_count": 2,
            "host_count": 2,
            "notes": null,
            "resolve_comment": null,
            "resolved_timestamp": null,
            "manual_severity": null,
            "manual_description": null,
            "xdr_url": "https://choicesolutions.xdr.us.paloaltonetworks.com/incident-view?caseId=32410",
            "starred": false,
            "hosts": [
                "ms-n19p1:4d53bfd1ccd945d2bffe2ca8c9e67510",
                "quickbooks-2:d085167ffe63ec78d9c525dafc22b145"
            ],
            "users": [
                "mikew",
                "pete"
            ],
            "incident_sources": [
                "XDR Agent"
            ],
            "rule_based_score": null,
            "manual_score": null,
            "wildfire_hits": 2,
            "alerts_grouping_status": "Enabled",
            "mitre_tactics_ids_and_names": null,
            "mitre_techniques_ids_and_names": null,
            "alert_categories": [
                "Malware"
            ]
                
        },
        
        alerts: {
            "external_id": "945146aaa6c811ea8c912c44fd2d2694",
            "severity": "high",
            "matching_status": "MATCHED",
            "end_match_attempt_ts": null,
            "local_insert_ts": 1591319166695,
            "bioc_indicator": null,
            "matching_service_rule_id": null,
            "attempt_counter": 0,
            "bioc_category_enum_key": null,
            "is_whitelisted": false,
            "starred": false,
            "deduplicate_tokens": null,
            "filter_rule_id": null,
            "mitre_technique_id_and_name": null,
            "mitre_tactic_id_and_name": null,
            "agent_version": null,
            "agent_device_domain": "ccare.local",
            "agent_fqdn": null,
            "agent_os_type": "Windows",
            "agent_os_sub_type": "6.1.7601",
            "agent_data_collection_status": null,
            "mac": null,
            "events": [
                {
                    "agent_install_type": "STANDARD",
                    "agent_host_boot_time": null,
                    "event_sub_type": null,
                    "module_id": null,
                    "association_strength": null,
                    "dst_association_strength": null,
                    "story_id": null,
                    "event_id": null,
                    "event_type": "File Event",
                    "event_timestamp": 1591319100024,
                    "actor_process_instance_id": null,
                    "actor_process_image_path": null,
                    "actor_process_image_name": null,
                    "actor_process_command_line": null,
                    "actor_process_signature_status": "N/A",
                    "actor_process_signature_vendor": "N/A",
                    "actor_process_image_sha256": null,
                    "actor_process_image_md5": null,
                    "actor_process_causality_id": null,
                    "actor_causality_id": null,
                    "actor_process_os_pid": null,
                    "actor_thread_thread_id": null,
                    "causality_actor_process_image_name": null,
                    "causality_actor_process_command_line": null,
                    "causality_actor_process_image_path": null,
                    "causality_actor_process_signature_vendor": "N/A",
                    "causality_actor_process_signature_status": "N/A",
                    "causality_actor_causality_id": null,
                    "causality_actor_process_execution_time": null,
                    "causality_actor_process_image_md5": null,
                    "causality_actor_process_image_sha256": null,
                    "action_file_path": "\\\\?\\C:\\WINPCACE\\Ansi837h.exe",
                    "action_file_name": "Ansi837h.exe",
                    "action_file_md5": null,
                    "action_file_sha256": "bdbc2d8c529fd30ab091f4740389ec45bd0d18b77234ef1a2f09f2e6062ad7d3",
                    "action_file_macro_sha256": null,
                    "action_registry_data": null,
                    "action_registry_key_name": null,
                    "action_registry_value_name": null,
                    "action_registry_full_key": null,
                    "action_local_ip": null,
                    "action_local_port": null,
                    "action_remote_ip": null,
                    "action_remote_port": null,
                    "action_external_hostname": null,
                    "action_country": "UNKNOWN",
                    "action_process_instance_id": null,
                    "action_process_causality_id": null,
                    "action_process_image_name": null,
                    "action_process_image_sha256": null,
                    "action_process_image_command_line": null,
                    "action_process_signature_status": "N/A",
                    "action_process_signature_vendor": "N/A",
                    "os_actor_effective_username": null,
                    "os_actor_process_instance_id": null,
                    "os_actor_process_image_path": null,
                    "os_actor_process_image_name": null,
                    "os_actor_process_command_line": null,
                    "os_actor_process_signature_status": "N/A",
                    "os_actor_process_signature_vendor": "N/A",
                    "os_actor_process_image_sha256": null,
                    "os_actor_process_causality_id": null,
                    "os_actor_causality_id": null,
                    "os_actor_process_os_pid": null,
                    "os_actor_thread_thread_id": null,
                    "fw_app_id": null,
                    "fw_interface_from": null,
                    "fw_interface_to": null,
                    "fw_rule": null,
                    "fw_rule_id": null,
                    "fw_device_name": null,
                    "fw_serial_number": null,
                    "fw_url_domain": null,
                    "fw_email_subject": null,
                    "fw_email_sender": null,
                    "fw_email_recipient": null,
                    "fw_app_subcategory": null,
                    "fw_app_category": null,
                    "fw_app_technology": null,
                    "fw_vsys": null,
                    "fw_xff": null,
                    "fw_misc": null,
                    "fw_is_phishing": "N/A",
                    "dst_agent_id": null,
                    "dst_causality_actor_process_execution_time": null,
                    "dns_query_name": null,
                    "dst_action_external_hostname": null,
                    "dst_action_country": null,
                    "dst_action_external_port": null,
                    "contains_featured_host": null,
                    "contains_featured_user": null,
                    "contains_featured_ip": null,
                    "image_name": null,
                    "container_id": null,
                    "cluster_name": null,
                    "referenced_resource": null,
                    "operation_name": null,
                    "identity_sub_type": null,
                    "identity_type": null,
                    "project": null,
                    "cloud_provider": null,
                    "resource_type": null,
                    "resource_sub_type": null,
                    "user_agent": null,
                    "user_name": "N/A"
                }
            ],
            "alert_id": "34671",
            "detection_timestamp": 1591319100024,
            "name": "WildFire Malware",
            "category": "Malware",
            "endpoint_id": "4d9e1846b04fdd33f6fc00d77e7eecbe",
            "description": "Suspicious executable detected",
            "host_ip": [
                "10.27.0.177"
            ],
            "host_name": "CRCorpBilling2",
            "mac_addresses": null,
            "source": "XDR Agent",
            "action": "POST_DETECTED",
            "action_pretty": "Detected (Post Detected)"
        },
        full: {
            "Company_ID": "CWMCustomer ID",
            "qty": 460,
            "ServerName": "Server_Name",
            "clock": "2022-01-01T05:00:00.000Z",
            "ItemName": "Item",
            "sku": "Item Sku",
            "key_": "Zabbix Key"
        },
        problemManagment: {
            "triggerid": 117325,
            "eventid": 111520143,
            "Problem": "C:: Disk space is critically low (used > 90%)",
            "HostName": "T4L-INTRA01",
            "count": 28
        },
        problems: {
            "date_time": "2022-01-28T16:06:10.000Z",
            "epriority": 5,
            "priority": 5,
            "status": 0,
            "eventid": 111691705,
            "triggerid": 163452,
            "host_name": "SOL-CTXSHD20",
            "hostid": 12037,
            "itemid": 338770,
            "item_name": "Zabbix agent availability",
            "problem_name": "Zabbix agent is not available (for 5m)",
            "acknowledged": 0
        },
        problemsCount: {
            "priority": 5,
            "PName": "Disaster",
            "count": 7
        },
        cwHash: {
            "summary": "Zabbix - High Alert on Trinity - TB-DR-Core1",
            "hash": 244,
            "Last Occur": "2022-01-27T23:43:23.090Z"
        },
        cwSLA: {
            "Ticket Num": 1259110,
            "Resolve By": "2021-10-21T09:22:51.243Z",
            "Hours Left": -2383,
            "Company": "Choice Cloud",
            "Summary Description": "Alert A160104: DFW-NTNX-CLUSTER-01: Warning: File Server internal network is not on the same subnet ",
            "Service Board": "Virtualization and Storage",
            "Status": "In Progress",
            "Ticket Owner First Name": "Darrell",
            "Ticket Owner Last Name": "Doty"
        },
        cwSLACount: {
            "Count(Ticket Num)": 88
        },
        cwDailyBackup: {
            "id": 1289048,
            "Date_Entered": "2022-01-27T01:05:39.047Z",
            "Company_Name": "A.J. Manufacturing Company, Inc.",
            "Summary": "Daily Backup Check - DFWAJMUEB01",
            "Status_Description": "Closed",
            "Backup": "Backup Success"
        },
        cwDailyBackupOffsite: {
            "id": 1289028,
            "Date_Entered": "2022-01-27T01:03:40.390Z",
            "Company_Name": "Choice Cloud",
            "Summary": "Offsite Daily Backup Check - ATLCCUEB02",
            "Status_Description": "Backup Remediation",
            "Backup": "Backup In Progress"
        },
        cwSku: {
            "Company_ID": "ACEAuditingComplianceEduc",
            "Company_Name": "ACE - Auditing, Compliance, & Education",
            "Company_RecID": 17677,
            "AGR_Name": "Managed Services - Hosted Cloud Environment",
            "SKU": "MS - Citrix Cloud Infra",
            "Description": "MS - Citrix Cloud Iinfrastructure",
            "CustomerDescription": "MS - Citrix Cloud Infrastructure\n",
            "AGD_Desc": "MS - Citrix Cloud Infrastructure\n",
            "AGR_Date_End": "2022-11-30T00:00:00.000Z",
            "AGD_Date": "2018-11-01T00:00:00.000Z",
            "AGD_Qty": 1,
            "AGD_Cancel": null,
            "AGD_Cost": 640,
            "Extended_Cost_Amount": [
                640,
                640
            ],
            "Date_Entered_UTC": "2018-10-31T16:15:33.223Z",
            "Last_Update": "2021-11-30T09:17:28.513Z"
        },
        newTicketPost: {
            "id": 1290116,
            "summary": "This is a test",
            "recordType": "ServiceTicket",
            "board": {
                "id": 44,
                "name": "Automation",
                "_info": {
                    "board_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/boards/44"
                }
            },
            "status": {
                "id": 847,
                "name": "Unassigned",
                "_info": {
                    "status_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/boards/44/statuses/847"
                }
            },
            "company": {
                "id": 16004,
                "identifier": "ChoiceCloud",
                "name": "Choice Cloud",
                "_info": {
                    "company_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/company/companies/16004",
                    "mobileGuid": "bf0d1ba9-8b3f-45cd-950d-54e51b4e8a56"
                }
            },
            "site": {
                "id": 20713,
                "name": "Main",
                "_info": {
                    "site_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/company/companies/16004/sites/20713",
                    "mobileGuid": "b3205e2b-a78a-4d15-85e3-ff543d7d6044"
                }
            },
            "siteName": "Main",
            "addressLine1": "7015 College Blvd.",
            "addressLine2": "Suite 300",
            "city": "Overland Park",
            "stateIdentifier": "KS",
            "zip": "66211",
            "country": {
                "id": 1,
                "name": "United States",
                "_info": {
                    "country_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/company/countries/1"
                }
            },
            "contact": {
                "id": 49194,
                "name": "Managed  Security",
                "_info": {
                    "mobileGuid": "d23eb753-68f9-4001-a3d5-74dfba9cf8a3",
                    "contact_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/company/contacts/49194"
                }
            },
            "contactName": "Managed  Security",
            "contactEmailAddress": "alienvault@choicesolutions.com",
            "team": {
                "id": 51,
                "name": "MS - Automation",
                "_info": {
                    "team_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/boards/44/teams/51"
                }
            },
            "priority": {
                "id": 10,
                "name": "MS 4 - Low",
                "sort": 13,
                "_info": {
                    "priority_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/priorities/10",
                    "image_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/priorities/10/image?lm=2015-11-17T15:14:51Z"
                }
            },
            "serviceLocation": {
                "id": 6,
                "name": "Remote",
                "_info": {
                    "location_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/locations/6"
                }
            },
            "source": {
                "id": 2,
                "name": "Customer Phone Call",
                "_info": {
                    "source_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/sources/2"
                }
            },
            "agreement": {
                "id": 1348,
                "name": "Managed Services",
                "_info": {
                    "agreement_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/finance/agreements/1348"
                }
            },
            "severity": "Medium",
            "impact": "Medium",
            "allowAllClientsPortalView": false,
            "customerUpdatedFlag": false,
            "automaticEmailContactFlag": false,
            "automaticEmailResourceFlag": false,
            "automaticEmailCcFlag": false,
            "closedFlag": false,
            "dateEntered": "2022-01-31T14:45:52Z",
            "enteredBy": "labtech",
            "approved": true,
            "estimatedExpenseCost": 0,
            "estimatedExpenseRevenue": 0,
            "estimatedProductCost": 0,
            "estimatedProductRevenue": 0,
            "estimatedTimeCost": 0,
            "estimatedTimeRevenue": 0,
            "billingMethod": "ActualRates",
            "subBillingMethod": "ActualRates",
            "resolveMinutes": 0,
            "resPlanMinutes": 0,
            "respondMinutes": 0,
            "isInSla": true,
            "hasChildTicket": false,
            "hasMergedChildTicketFlag": false,
            "billTime": "DoNotBill",
            "billExpenses": "NoDefault",
            "billProducts": "NoDefault",
            "locationId": 2,
            "businessUnitId": 19,
            "mobileGuid": "5e9a16ac-9275-4a6a-ab94-485570474824",
            "sla": {
                "id": 6,
                "name": "Managed Services - 24x7",
                "_info": {
                    "sla_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/SLAs/6"
                }
            },
            "currency": {
                "id": 7,
                "symbol": "$",
                "currencyCode": "USD",
                "decimalSeparator": ".",
                "numberOfDecimals": 2,
                "thousandsSeparator": ",",
                "negativeParenthesesFlag": true,
                "displaySymbolFlag": true,
                "currencyIdentifier": "USD",
                "displayIdFlag": false,
                "rightAlign": false,
                "name": "US Dollars",
                "_info": {
                    "currency_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/finance/currencies/7"
                }
            },
            "_info": {
                "lastUpdated": "2022-01-31T14:45:52Z",
                "updatedBy": "labtech",
                "activities_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/sales/activities?conditions=ticket/id=1290116",
                "scheduleentries_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/schedule/entries?conditions=type/id=4 AND objectId=1290116",
                "documents_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/system/documents?recordType=Ticket&recordId=1290116",
                "configurations_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/tickets/1290116/configurations",
                "tasks_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/tickets/1290116/tasks",
                "notes_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/service/tickets/1290116/notes",
                "products_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/procurement/products?conditions=chargeToType='Ticket' AND chargeToId=1290116",
                "timeentries_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/time/entries?conditions=(chargeToType='ServiceTicket' OR chargeToType='ProjectTicket') AND chargeToId=1290116",
                "expenseEntries_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/expense/entries?conditions=(chargeToType='ServiceTicket' OR chargeToType='ProjectTicket') AND chargeToId=1290116"
            },
            "customFields": [
                {
                    "id": 24,
                    "caption": "Doc Request",
                    "type": "Checkbox",
                    "entryMethod": "EntryField",
                    "numberOfDecimals": 0
                },
                {
                    "id": 26,
                    "caption": "Backup ",
                    "type": "Text",
                    "entryMethod": "List",
                    "numberOfDecimals": 0
                },
                {
                    "id": 27,
                    "caption": "Not Advised",
                    "type": "Checkbox",
                    "entryMethod": "EntryField",
                    "numberOfDecimals": 0
                },
                {
                    "id": 30,
                    "caption": "QBR Review",
                    "type": "Checkbox",
                    "entryMethod": "EntryField",
                    "numberOfDecimals": 0
                },
                {
                    "id": 31,
                    "caption": "Escalate To",
                    "type": "Text",
                    "entryMethod": "List",
                    "numberOfDecimals": 0
                },
                {
                    "id": 32,
                    "caption": "Customer Contact",
                    "type": "Text",
                    "entryMethod": "List",
                    "numberOfDecimals": 0
                },
                {
                    "id": 34,
                    "caption": "Emergency Change Control",
                    "type": "Checkbox",
                    "entryMethod": "EntryField",
                    "numberOfDecimals": 0
                },
                {
                    "id": 37,
                    "caption": "Noise Cleanup",
                    "type": "Checkbox",
                    "entryMethod": "EntryField",
                    "numberOfDecimals": 0
                }
            ]
        },
        cwTicketNotes: 
        {
            "id": 2176217,
            "ticketId": 1290116,
            "text": "This is a test",
            "detailDescriptionFlag": true,
            "internalAnalysisFlag": false,
            "resolutionFlag": false,
            "issueFlag": false,
            "member": {
                "id": 337,
                "identifier": "labtech",
                "name": "LabTech Software",
                "_info": {
                    "member_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/system/members/337",
                    "image_href": "https://portal.choicecloud.com/v4_6_release/apis/3.0/system/members/337/image?lm=2021-11-05T16:37:00Z"
                }
            },
            "dateCreated": "2022-01-31T14:45:52Z",
            "createdBy": "labtech",
            "internalFlag": true,
            "externalFlag": true,
            "_info": {
                "lastUpdated": "2022-01-31T14:45:52Z",
                "updatedBy": "labtech"
            }
        },
        workstations:{
            "computerid": 1137,
            "clientID": 18,
            "name": "EVT1VM",
            "Company_RecID": 343,
            "OS": "Microsoft Windows Server 2008 R2 Standard x64",
            "NumProc": 1,
            "IsServer": 1,
            "sku": "MS - Windows Best Effort Server"
        },
        patchRebootAudit:{
            "ComputerID": 16094,
            "Client": "A.J. Manufacturing Company, Inc.",
            "Location": "PO - Workstations",
            "Computer": "AJ-BEND"
        },
        patchAudit: {
            "HotfixID": "67c10bc1-98c1-4dd1-baec-ed9f40b6bce7",
            "ComputerID": 15256,
            "Client": "CentriNet Corp",
            "Location": "DFW - QTS",
            "computer": "DAL-HV01",
            "OS": "Windows Server 2016 x64",
            "MAC": "64-31-50-51-0F-14",
            "IPAddress": "10.100.90.165",
            "DeviceCompliance": 16.67,
            "ApprovalPolicy": "Approve",
            "PatchState": "Not Attempted",
            "vPro": 0,
            "WOL": 0,
            "Connected": 0,
            "IdleTime": 883592,
            "Idle": 0,
            "DaytimePatching": 0,
            "PatchJobRunning": 0,
            "PendingReboot": 0,
            "LastPatchJobFailed": 0,
            "NoPatchInventory": 0,
            "WSUSEnabled": 0,
            "OutofDateWUA": 0,
            "MissingBaseline": 0,
            "Stage": 2,
            "PatchJobs": 0,
            "MaintenanceBegin": "2022-08-17T20:29:46.000Z",
            "MaintenanceDuration": -1,
            "MaintenanceMode": -1
        },
        activeDirUsers: {
            "AccountName": "ACEPRES",
            "AccountControls": 512,
            "LastLogon": "2022-02-14T17:09:06.000Z",
            "FirstName": "ACEPRES",
            "LastName": null,
            "Email": "jack.neil@hank.ai",
            "DomainAdministrator": 0,
            "NTDomain": "CCACE01",
            "InfrastructureServerID": 13928,
            "ServerName": "DFWACEDC01",
            "ClientID": 67,
            "ExternalID": 17677
        },
        spla: {
            "name": "T4L-POS",
            "clientname": "Tickets for Less",
            "processors": "24",
            "LastRun": "8/1/2022 1:31:05 AM",
            "sqlversions": "MSSQLSERVER Standard Edition 14.0.1000.169;"
        },
        badHealth: {
            "FailedStandardId": 1,
            "StandardGuid": "16e5065c-d6e6-11e3-b785-6431502512ea",
            "StandardName": "Server: Operating System",
            "TargetType": 1,
            "TargetId": 1137,
            "DeviceName": "Trinity Bank NA / DATA CENTER - FW / EVT1VM",
            "VendorId": 0,
            "ProductId": 0,
            "Priority": 5,
            "PossibilityGenerated": 0,
            "Ignored": 0,
            "DetectedDate": "2022-08-17T08:23:29.000Z",
            "LastUpdate": "2022-08-17T08:23:29.000Z",
            "FailedMessage": "EVT1VM has Microsoft Windows Server 2008 R2 Standard x64 (6.1) installed and does not meet the minimum Server 2008 R2 standard."
        },
 



    }
};

const outputFile = './routes/docs/swagger-output.json';
const endpointsFiles = ['./index.js'];

/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as: index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);