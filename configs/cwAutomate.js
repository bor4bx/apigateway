const axios = require('axios');

const cwAutomateConfig = {
    "user": process.env.CWA_user,
    "host": process.env.CWA_host,
    "port": process.env.CWA_port,
    "password": process.env.CWA_password,
    "database": process.env.CWA_database,
    "connectionslimit": 10
};



const queries = {
    computers: `
    SELECT DISTINCT
      clients.ExternalID as Company_RecID,
      computers.computerid,
      computers.clientID,
      clients.name as clientName,
      locations.name as locationName,
      computers.name as computerName,
      computers.OS,
      (SELECT SUM(enabled) FROM inv_processor WHERE computers.computerid = inv_processor.computerid) AS "NumProc",
      inv_operatingsystem.Server AS 'IsServer',
      case
        when (inv_operatingsystem.Server = 0 or inv_operatingsystem.Server is null)   and (OS like '%Windows 10%' or OS like '%Windows 11%') then 'MS - Windows Desktop'
        when inv_operatingsystem.Server = 0  and OS like '%OS X%' then 'MS - Mac Desktop'
        when inv_operatingsystem.Server = 1 and (OS like '%Server 2019%' or OS like '%Server 2016%' or OS like '%Server 2012%') then 'MS - Windows Server'
        when inv_operatingsystem.Server = 0  and OS like '%Linux%' then 'MS - Linux Server' 
        when inv_operatingsystem.Server = 1 and OS like '%Server%' then 'MS - Windows Best Effort Server'
        else 'MS - Best Effort Desktop'
      end as sku
    FROM  computers 
        LEFT JOIN inv_operatingsystem ON computers.ComputerID = inv_operatingsystem.ComputerID
        LEFT JOIN clients ON computers.clientID = clients.clientID
        LEFT JOIN locations ON computers.locationid = locations.locationid
        LEFT JOIN inv_processor ON computers.computerid = inv_processor.computerId`,
  computersAV: `
        SELECT 
              clients.ExternalID as Company_RecID,
              computers.computerid,
              computers.clientID,
              clients.name as clientName,
              locations.name as locationName,
              computers.name as computerName,
              computers.OS,
            1 AS "NumProc",
              inv_operatingsystem.Server AS 'IsServer',
              case
                when inv_operatingsystem.Server = 1 and (computers.VirusScanner = 287 or computers.VirusScanner = 286) then 'SECaaS - EPPAAS-S'
                when inv_operatingsystem.Server = 0 and (computers.VirusScanner = 287 or computers.VirusScanner = 286) then 'SECaaS - EPPAAS-D'
                else ''
        end as sku
         FROM labtech.computers
        join virusscanners vs on vs.VScanID = VirusScanner
        LEFT JOIN inv_operatingsystem ON computers.ComputerID = inv_operatingsystem.ComputerID
        LEFT JOIN clients ON computers.clientID = clients.clientID
        LEFT JOIN locations ON computers.locationid = locations.locationid
        where 
        computers.VirusScanner = 287 or computers.VirusScanner = 286`,
    computerUsers: `SELECT computerID,computers.name,UserAccounts FROM computers`,
    notCheckedIn: `
    SELECT DISTINCT
      computers.computerid,
      computers.clientID,
      clients.name as clientName,
      clients.ExternalID as Company_RecID,
      locations.name as locationName,
      computers.name as ComputerName,
      computers.OS,
      (SELECT SUM(enabled) FROM inv_processor WHERE computers.computerid = inv_processor.computerid) AS "NumProc",
      inv_operatingsystem.Server AS 'IsServer',
      case
      when inv_operatingsystem.Server = 0  and OS like '%Windows 10%' then 'MS - Windows Desktop'
      when inv_operatingsystem.Server = 0  and OS like '%OS X%' then 'MS - Mac Desktop'
      when inv_operatingsystem.Server = 1 and (OS like '%Server 2019%' or OS like '%Server 2016' or OS like '%Server 2012%') then 'MS - Windows Server'
      when inv_operatingsystem.Server = 0  and OS like '%Linux%' then 'MS - Linux Server' 
      when inv_operatingsystem.Server = 1 and OS like '%Server%' then 'MS - Windows Best Effort Server'
      else 'MS - Best Effort Desktop'
      end as sku
    FROM  computers 
      LEFT JOIN inv_operatingsystem ON computers.ComputerID = inv_operatingsystem.ComputerID
      LEFT JOIN clients ON computers.clientID = clients.clientID
      LEFT JOIN locations ON computers.locationid = locations.locationid
      LEFT JOIN inv_processor ON computers.computerid = inv_processor.computerId
    where 
      LastContact < date_sub(now(),interval 30 day)
    order by company_recID`,
    patchRebootAudit: `
    SELECT 
      clients.ExternalID as Company_RecID,
      clients.clientID,
      clients.name AS clientName,
      pd.ComputerID,
      pd.Location,
      pd.Computer as computerName
    FROM labtech.v_patchdeploymentinfo pd
      LEFT JOIN computers ON pd.ComputerID = computers.ComputerID
      LEFT JOIN clients ON computers.clientID = clients.clientID
    where 
      pd.pendingReboot !=0
      and
      pd.ApprovalPolicy = 'Approve'
      group by pd.ComputerID
      order by pd.Client`,
    patchAudit: `
    SELECT 
      clients.ExternalID as Company_RecID, 
      clients.clientID,
      clients.Name AS clientName,
      computers.name, 
      dep.* 
    FROM labtech.patchingdevicecompliancedata dep
     LEFT JOIN computers ON dep.ComputerID = computers.ComputerID
     LEFT JOIN clients ON computers.clientID = clients.clientID
	where 
     (MicrosoftCompliance < 100 and MicrosoftApprovedCount !=0)
     or 
     (ThirdPartyCompliance < 100 and ThirdPartyApprovedCount !=0)
    order by clients.ExternalID, MicrosoftCompliance    `,
    patchServerDetailAudit: `
    SELECT clients.ExternalID as Company_RecID,
     computers.ComputerID,
     clients.name AS clientName,
     dp.location,
     dp.computer,
     dp.OS,
     dp.PatchState,
     dp.PendingReboot,
     dp.hotfixID,
     data.kbID,
     data.Description,
     data.SupportURL,
     data.CategoryName,
     data.OS
    FROM labtech.v_patchdeploymentinfo  dp
      LEFT join labtech.hotfixdata data on data.HotFixID = dp.HotFixID
      LEFT JOIN computers ON dp.ComputerID = computers.ComputerID
      LEFT JOIN clients ON computers.clientID = clients.clientID
    where 
     DeviceCompliance < 100
     and
     ApprovalPolicy = 'Approve'
     and
     (dp.PatchState != 'Installed' and dp.PendingReboot= 0)
    group by computer,HotFixID,kbID`,
    patchHealthReport: `
    SELECT clients.ExternalID as Company_RecID,
      clients.clientID,
      clients.Name AS clientName,
      count(Distinct if(dep.OS like '%Server%',dep.computer, NULL)) as TotalServers,
      count(Distinct if(dep.OS NOT like '%Server%',dep.computer, NULL)) as TotalWorkstations,
      count(DISTINCT if(DeviceCompliance=100, if(dep.OS like '%Server%',dep.computer, NULL),NULL)) as CompliantServerCount ,
      count(DISTINCT if(DeviceCompliance<100, if(dep.OS like '%Server%',dep.computer, NULL), NULL)) as NonCompliantServerCount,
      CAST((count(DISTINCT if(DeviceCompliance<100, if(dep.OS like '%Server%',dep.computer, NULL), NULL)) / count(Distinct if(dep.OS like '%Server%',computer, NULL)) * 100) AS DECIMAL(12,2)) as PercentServerNonCompliant,
      count(DISTINCT if(DeviceCompliance=100, if(dep.OS NOT like '%Server%',dep.computer, NULL),NULL)) as CompliantWorkstationsCount ,
      count(DISTINCT if(DeviceCompliance<100, if(dep.OS NOT like '%Server%',dep.computer, NULL), NULL)) as NonWorkstationsCount,
      CAST((count(DISTINCT if(DeviceCompliance<100, if(dep.OS NOT like '%Server%',dep.computer, NULL), NULL)) / count(Distinct if(dep.OS NOT like '%Server%',computer, NULL)) * 100) AS DECIMAL(12,2))  as PercentWorkstationNonCompliant
    FROM labtech.v_patchdeploymentinfo  dep
      LEFT JOIN computers ON dep.ComputerID = computers.ComputerID
      LEFT JOIN clients ON computers.clientID = clients.clientID
    where 
      dep.ApprovalPolicy = 'Approve'
      and
      dep.Client != 'Tanner Health System'
      group by dep.client
    order by PercentServerNonCompliant desc`,
    patchfails: `
    SELECT 
      clients.ExternalID as Company_RecID, 
      clients.Name AS clientName,
      clients.clientID,
      dep.* 
    FROM labtech.noncompliantpatches dep
      LEFT JOIN clients ON dep.clientID = clients.clientID`,
    activeDirUsers: `
    Select 
      clients.ExternalID as Company_RecID,
      clients.clientID,
      clients.Name AS clientName,
      users.AccountName,users.AccountControls,
      users.LastLogon,
      users.FirstName,
      users.LastName,
      users.email,
      users.DomainAdministrator,
      domain.NTDomain,
      domain.InfrastructureServerID,
      computers.name as ServerName
    from (select *,SUBSTRING_INDEX(ObjectSid, "-",7) as domainSid from labtech.plugin_ad_users) as users 
      join labtech.plugin_ad_domains domain on users.domainSid = domain.ObjectSid
      join labtech.computers on domain.InfrastructureServerID=computers.computerID
      join labtech.clients on computers.ClientID=clients.ClientID`,
    spla: "SELECT clients.ExternalID as Company_RecID,clients.clientID,clients.name AS clientName,v_extradatacomputers.name,`Logical Processors` AS processors,v_extradatacomputers.LastRun,v_extradatacomputers.`SQL Versions` AS sqlversions FROM v_extradatacomputers  LEFT JOIN computers ON v_extradatacomputers.computerid = computers.computerid LEFT JOIN clients ON computers.clientid = clients.clientid LEFT JOIN v_extradataclients ON clients.clientid = v_extradataclients.clientid WHERE v_extradatacomputers.IsSQL = 1 AND `SQL Versions` NOT LIKE '%Express%' AND `SQL Versions` NOT LIKE '%Null%' AND v_extradataclients.SPLA LIKE '1' AND computers.os LIKE '%Server%'",
    badHealth: `
    SELECT 
      cl.ExternalID as Company_RecID,
      cl.ClientID,
      cl.name AS clientName,
      c.name as ComputerName,
      c.ComputerID,
      f.* 
    FROM labtech.plugin_sap_failedstandards f
      join labtech.computers c on f.targetId = c.ComputerID
      join clients cl on c.ClientID = cl.ClientID
    order by cl.ClientID`
}


module.exports = {

    queries,
    cwAutomateConfig 
}