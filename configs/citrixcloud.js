const axios = require('axios');
//const { RESERVED_FUNCTIONS } = require('swagger-autogen/src/statics');


const {
    globalGatewayCache
} = require("./cache.js");



async function getBaerer() {
    if(globalGatewayCache.has("citrix_Baerer")){
        console.log("returning citrix Baerer from Cache");
        tok = globalGatewayCache.get("citrix_Baerer")
        return(tok);
    }
    else {
        try {
            const config = {
                method: 'post',
                url: 'https://trust.citrixworkspacesapi.net/root/tokens/clients',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Host': 'trust.citrixworkspacesapi.net',
                    'Accept-Encoding': 'gzip, deflate, br'
                },
                data: {
                    clientId: process.env.CTX_Cloud_clientId,
                    clientSecret: process.env.CTX_Cloud_clientSecret
                }
            };
            let ret = await axios(config);
            let token = ret.data.token
            globalGatewayCache.set("citrix_Baerer", token);
            return (token);
        } catch (error) {
            console.error(error);
        }
    }
};

async function citrixCloudLic() {
    const citrixToken = await getBaerer();
    var dateObj = new Date();
    var month = ("0" + (dateObj.getMonth() + 1)).slice(-2);
    var year = dateObj.getUTCFullYear();
    var cvadDate = year + '-' + month;
    const cvadUrl = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/licenseusages/deployments/cloud/memberships/csp/products/cvad?date=';

    if(globalGatewayCache.has("citrix_CloudLic")){
        console.log("Returning Cloud Lic from Cache");
        let lic = globalGatewayCache.get("citrix_CloudLic")
        return(lic);
    }
    else {
        console.log("Getting a Fresh Citrix lic info")
        try{
            let conf = {
                method: 'get',
                url: cvadUrl + cvadDate,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'CwsAuth Bearer=' + citrixToken,
                    'Accept-Encoding': 'gzip, deflate, br'
                }
            };

            let response1 = await axios(conf);
            let data = response1.data;
            globalGatewayCache.set("citrix_CloudLic", data);
            return(data);

        } catch(error) {
            /**************************************************************************
            TODO: If error is "Invalid bearer token" then expire token cache and re-try
            {
                "Error": {
                    "Type": "CWSAuthException",
                    "Message": "Invalid bearer token."
                } 
            } 
            **************************************************************************/
          console.log(error);
        }
    }
}

module.exports = {
    getBaerer,citrixCloudLic
};



