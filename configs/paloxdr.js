const axios = require('axios');
//const { RESERVED_FUNCTIONS } = require('swagger-autogen/src/statics');
const NodeCache = require( "node-cache" );
const paloCache = new NodeCache( { stdTTL: 100, checkperiod: 120 } );

const apiKey = 'ccit0c4WhpIS1WKa5tX241jS3R9VBqLvxkg5R8qKPrPxUIfdVosmZQ9UzTs9BzAWDHCyJfuj9iiSpziGW6uCuCJxxyUEDs7gPwXXJRbDGKY8Tx6QQ8o1Fgq99QQJSUit'
const apiKeyId = '3'
const xdrBaseFqdn = 'https://api-choicesolutions.xdr.us.paloaltonetworks.com'
const getEndpoints = '/public_api/v1/endpoints/get_endpoint'
// const {
//     globalGatewayCache
// } = require("../../configs/cache.js");

async function getendpointDetails(endpointID){
    const endpointConf = {
            method: 'post',
            url: `https://api-choicesolutions.xdr.us.paloaltonetworks.com/public_api/v1/endpoints/get_endpoint/`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Accept-Encoding': 'gzip, deflate, br',
                'x-xdr-auth-id': apiKeyId,
                'Authorization': apiKey
            },
            data: {
                "request_data": {
                    "filters": [
                        {
                            "field":"endpoint_id_list",
                            "operator":"in",
                            "value":[endpointID]
                        }
                    ],

                }
            } 
               
    }
    try{
        const r = await axios(endpointConf);
        
        return(r);
    } catch (error) {
        console.log(error);
        return('error')

    }
}
async function getincidents(){
    try {
        const incidentConf = {
            method: 'post',
            url: `https://api-choicesolutions.xdr.us.paloaltonetworks.com/public_api/v1/incidents/get_incidents/`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Accept-Encoding': 'gzip, deflate, br',
                'x-xdr-auth-id': apiKeyId,
                'Authorization': apiKey
            },
            data: {
                "request_data": {
                    "filters": [
                        {
                            "field": "status",
                            "operator": "in",
                            "value": [ "new", "under_investigation"]
                        }
                    ],

                }
            }
        }
        const r = await axios(incidentConf);
        
        return(r);
    } catch (error) {
        console.log(error);
    }
}

async function getincidentDetails(){
        let incidents = await getincidents()
        console.log(incidents.data.reply.result_count);
        var details;
        try {
            for (var i=0; i< 2; i++){ //incidents.data.reply.result_count; i++){
                var inc = incidents.data.reply.incidents[i];
                console.log(inc['incident_id']);
                let id = inc['incident_id']
// ***
            
                const incidentConf = {
                    method: 'post',
                    url: `https://api-choicesolutions.xdr.us.paloaltonetworks.com/public_api/v1/incidents/get_incident_extra_data/`,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'x-xdr-auth-id': apiKeyId,
                        'Authorization': apiKey
                    },
                    data: {
                        'request_data': {
                            'incident_id': inc['incident_id']
                        }                        
                    }
                }

                //details.push(id)
                const r = await axios(incidentConf)
                try{
                    details.push(r.data.reply);
                }
                catch(error){console.log('push error ' + error)}
            }
                //console.log(r.data.reply.alerts);
                // details.push.inc['incident_id'].push(r.data.reply.incident);
                // details.push.inc['incidnet_id'].push(r.data.reply.alerts);
        } catch (error) {
            console.log(error);
        }

// ***
return(r);
        };
        
        
            

 


module.exports = { 
    getendpointDetails,getincidents,getincidentDetails
    
};