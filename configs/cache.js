const NodeCache = require( "node-cache" );
const globalGatewayCache = new NodeCache( { stdTTL: 100, checkperiod: 120 } );

module.exports = {globalGatewayCache};

/************************************************************************************************************************************
 Global Difined Cache Varriables:

    globalGatewayCache.get("manage_Huntress"); Userfield_7]: Company_RecID;
    globalGatewayCache.get("manage_ID2RecID"; Company_ID: Company_RecID
    globalGatewayCache.get("manage_RecID2ID"); Company_RecID: Company_ID
    globalGatewayCache.get('manage_'+req.params.sql); ### Cache of the  CW Managage get request based on the requested sql varialbe 
    globalGatewayCache.get("automate_"+req.params.sql) ### Cache of the CW Automate get request based on the requested sql varialbe
    globalGatewayCache.set("manage_Name2RecandID", companyName) <CompanyName>: [
                                                                'company_ID': Company_ID,  ### will have set if in Zabbix
                                                                'company_RecID': Company_RecID ## will have set if in Automate
                                                            ]
    globalGatewayCache.get("device2Company"); <device name>: [
                                                                'company_ID': Company_ID,  ### will have set if in Zabbix
                                                                'company_RecID': Company_RecID ## will have set if in Automate
                                                            ]
    globalGatewayCache.get("zabbix_"+req.params.sql); ### Cache of the  Zabbix get request based on the requested sql varialbe
    globalGatewayCache.get("citrix_CloudLic"); ### Cache of the  Citrix Lic data
    globalGatewayCache.get("citrix_Baerer");  ## Cache of the Citrix Baerer token


*****************************************************************************************************************************************/