// Connections
console.log('in config file' + process.env)


const cwMConfig = {
    user: process.env.CWM_DATABASE_USER,
    password: process.env.CWM_DATABASE_PASSWORD,
    database: process.env.CWM_DATABASE_NAME,
    server: process.env.CWM_DATABASE_HOST,
    encrypt: false,
}

//Date Variables

var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1; // months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();
var cvadDate = year + '-' + month;
var sevenDaysAgo = dateObj.getUTCDate() - 7;
var date = new Date().toISOString().substr(0, 19).replace('T', ' ')
var firstofthemonth = new Date(dateObj.getUTCFullYear(),dateObj.getUTCMonth(), 01).toISOString().substr(0, 19).replace('T', ' ')
var frstofLastMonth = dateObj.getUTCFullYear()+ "-" + dateObj.getUTCMonth() + "-" + '01'


//Sql Statements

const queries = {
    cwHash: `SELECT TOP 20
        summary,
        count( HASHBYTES('md5', Summary)) as hash,
        Max(Date_Entered_UTC) as 'Last Occur'
        FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
        where 
        Billing_unit_desc like '%Man%'
        and
        Board_Name not in ('MS Presales', 'Patching', 'Internal MS')
        and
        Date_Entered_UTC > '${year}-${month}-1' and Date_Entered_UTC <= '${year}-${month}-${day}'
        and 
        summary not like 'CS - ScreenConnect - Disc%'
        and
        summary not like '%BITS%'
        and 
        summary not like '%Backup%'
        and
        hours_actual > .25
        group by HASHBYTES('md5', Summary),summary
        order by hash desc`,
    cwSME: `SELECT 
    t.[Company_RecID] as Company_RecID
    ,c.Company_Name as CompanyName
    ,CONCAT(m.First_name, ' ', m.Last_name) as Tech
    ,r.Role_Desc as Role
FROM [cwwebapp_choice].[dbo].[Company_Team] t
join member m on m.Member_RecID = t.Member_RecID
join Company c on c.Company_RecID = t.Company_RecID
join Team_Role r on r.Team_Role_RecID = t.Team_Role_Recid
where 
t.Team_Role_Recid in (2,7)
and 
c.Company_Status_RecID = 25 
or c.Company_RecID in (2, 16004, 22484)
order by Company_RecID,Role`,

    cwHours: `SELECT TOP 50
        summary,
        company_name,
        count( HASHBYTES('md5', Summary)) as hash,
        sum(hours_actual) as hours,
        Max(Date_Entered_UTC) as 'Last Occur'
        FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
        where 
        Billing_unit_desc like '%Man%'
        and
        Board_Name not in ('MS Presales', 'Patching', 'Internal MS')
        and
        Date_Entered_UTC > '${year}-${month}-1' and Date_Entered_UTC <= '${year}-${month}-${day}'
        and 
        summary not like 'CS - ScreenConnect - Disc%'
        and
        summary not like '%BITS%'
        and
        summary not like '%Backup%'
        and
        hours_actual > .25
        group by Company_name,hours_actual,HASHBYTES('md5', Summary),summary
        order by hours desc, hash desc`,
        
    cwNewHours: `SELECT [SR_Service_RecID]
    ,[Company_ID]
    ,[Charge_To_RecID]
    ,[Charge_To_Type]
    ,[Member_RecID]
    ,[Member_ID]
    ,[AGR_Name]
    ,[Time_Start]
    ,[Time_End]
    ,[Hours_Deduct]
    ,[Hours_Actual]
    ,[BillableOption]
    ,[Notes_Markdown]
    ,[Internal_Note]
    ,[Hours_Bill]
    ,[Last_Update_UTC]
    ,[Updated_By]
    ,[Date_Entered_UTC]
    ,[Entered_By]
    ,[Company_Name]
    ,[Member_Name]
    ,[Ticket_Summary]
FROM [cwwebapp_choice].[dbo].[v_api_collection_time_entry]
where Charge_To_Type = 'ServiceTicket'
and Member_RecID NOT IN (206,327)
and Time_Start > '${frstofLastMonth}' and Time_Start <= '${date}'
and Hours_Actual > .25`,

cwSLA2: `    
SELECT TOP 100000
*
FROM [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets] WITH(NOLOCK)
INNER JOIN [cwwebapp_choice].[dbo].[v_rpt_Service] WITH(NOLOCK)  ON [cwwebapp_choice].[dbo].[v_rpt_Service].[TicketNbr]=[cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[TicketNum]
INNER JOIN [cwwebapp_choice].[dbo].[v_rpt_Company] with(NOLOCK) on [cwwebapp_choice].[dbo].[v_rpt_Company].[Company_RecID] = [cwwebapp_choice].[dbo].[v_rpt_Service].[company_recid]
WHERE  (NOT([cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[Status] IN ('--> Escalate to PS','Backup Remediation','Change Request Submitted')))
and
[cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ServiceBoard] != 'Change Management'
ORDER BY [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ResolveBy] ASC;
    `,
    cwSLA: `    
    SELECT TOP 100000
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[TicketNum] AS 'Ticket Num',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ResolveBy] AS 'Resolve By',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[HoursLeft] AS 'Hours Left',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[Company] AS 'Company',
    [cwwebapp_choice].[dbo].[v_rpt_Company].[Company_RecID] as 'Company_RecID',
    [cwwebapp_choice].[dbo].[v_rpt_Company].[Company_ID] as 'Company_ID',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[SummaryDescription] AS 'Summary Description',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ServiceBoard] AS 'Service Board',
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[Status] AS 'Status',
    [cwwebapp_choice].[dbo].[v_rpt_Service].[Ticket_Owner_First_Name] AS 'Ticket Owner First Name',
    [cwwebapp_choice].[dbo].[v_rpt_Service].[Ticket_Owner_Last_Name] AS 'Ticket Owner Last Name'
    FROM [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets] WITH(NOLOCK)
    INNER JOIN [cwwebapp_choice].[dbo].[v_rpt_Service] WITH(NOLOCK)  ON [cwwebapp_choice].[dbo].[v_rpt_Service].[TicketNbr]=[cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[TicketNum]
    INNER JOIN [cwwebapp_choice].[dbo].[v_rpt_Company] with(NOLOCK) on [cwwebapp_choice].[dbo].[v_rpt_Company].[Company_RecID] = [cwwebapp_choice].[dbo].[v_rpt_Service].[company_recid]
    WHERE  (NOT([cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[Status] IN ('--> Escalate to PS','Backup Remediation','Change Request Submitted')))
    and
    [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ServiceBoard] != 'Change Management'
    ORDER BY [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ResolveBy] ASC;
        `,
    cwSLACount: `
        SELECT TOP 100000
        COUNT([cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[TicketNum]) AS 'Count(Ticket Num)'
        FROM [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets] WITH(NOLOCK)
        INNER JOIN [cwwebapp_choice].[dbo].[v_rpt_Service] WITH(NOLOCK)  ON [cwwebapp_choice].[dbo].[v_rpt_Service].[TicketNbr]=[cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[TicketNum]
        WHERE  (NOT([cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[Status] IN ('--> Escalate to PS','Backup Remediation','Change Request Submitted')))
        and [cwwebapp_choice].[dbo].[v_ChoiceSolutions_SLAViolatedTickets].[ServiceBoard] != 'Change Management'
        `,
    cwDailyBackup: `
        SELECT [dbo].[v_rpt_Service].[SR_Service_RecID] AS 'id', 
        [dbo].[v_rpt_Service].[date_entered] AS 'Date_Entered', 
        [dbo].[v_rpt_Service].[company_name] AS 'Company_Name', 
        [dbo].[v_rpt_Service].[Summary] AS 'Summary', [dbo].[v_rpt_Service].[status_description] AS 'Status_Description', 
        [dbo].[v_SR_Service_Custom_Fields].[Backup ] AS 'Backup'
            FROM [dbo].[v_rpt_Service] WITH(NOLOCK)
            LEFT OUTER JOIN [dbo].[v_SR_Service_Custom_Fields] WITH(NOLOCK)  ON [dbo].[v_SR_Service_Custom_Fields].[SR_Service_RecID]=[dbo].[v_rpt_Service].[SR_Service_RecID]
            WHERE  [dbo].[v_rpt_Service].[date_entered] >= dateadd(day,datediff(day,1,GETDATE()),0)
            and [dbo].[v_rpt_Service].[date_entered] < dateadd(day,datediff(day,0,GETDATE()),0)
            AND ([dbo].[v_rpt_Service].[Summary] LIKE ISNULL('Daily Backup Check', '') + ISNULL('%',''))
            ORDER BY [dbo].[v_rpt_Service].[company_name] ASC;`,
    cwActiveMSskus: `
    SELECT Distinct
        IV_Item.Item_ID as SKU
        FROM AGR_Detail AGR_Detail
            left join AGR_Header on AGR_Detail.AGR_Header_RecID = AGR_Header.AGR_Header_RecID
            left join Company on AGR_Header.Company_RecID = Company.Company_RecID
            left join iv_item on AGR_Detail.IV_Item_RecID = IV_Item.IV_Item_RecID
        WHERE AGR_Detail.AGR_Header_RecID IN 
            (
                SELECT agr_header_recid FROM AGR_Header
                WHERE agr_type_recid in (19,45,60)
                AND (agr_date_end >= GETDATE() or agr_date_end is NULL)
            )
            AND AGR_Detail.AGD_Cancel is NULL
            AND AGR_Header.AGR_Cancel_Flag = 0
            and Company.Company_Status_RecID != 21`,
    cwDailyBackupOffsite: `
       SELECT [dbo].[v_rpt_Service].[SR_Service_RecID] AS 'id',
                [dbo].[v_rpt_Service].[date_entered] AS 'Date_Entered',
                [dbo].[v_rpt_Service].[company_name] AS 'Company_Name',
                [dbo].[v_rpt_Service].[Summary] AS 'Summary',
                [dbo].[v_rpt_Service].[status_description] AS 'Status_Description',
                [dbo].[v_SR_Service_Custom_Fields].[Backup ] AS 'Backup'
        FROM [dbo].[v_rpt_Service] WITH(NOLOCK)
        LEFT OUTER JOIN [dbo].[v_SR_Service_Custom_Fields] WITH(NOLOCK) ON [dbo].[v_SR_Service_Custom_Fields].[SR_Service_RecID]=[dbo].[v_rpt_Service].[SR_Service_RecID]
        WHERE  [dbo].[v_rpt_Service].[date_entered] >= dateadd(day,datediff(day,1,GETDATE()),0)
        and [dbo].[v_rpt_Service].[date_entered] < dateadd(day,datediff(day,0,GETDATE()),0)
        AND ([dbo].[v_rpt_Service].[Summary] LIKE ISNULL(ISNULL('%', '') + ISNULL('Offsite Daily Backup Check',''), '') + ISNULL('%','')) `,
    cwSku: `SELECT Company.Company_ID
                ,Company.Company_Name
                ,AGR_Header.Company_RecID
                ,AGR_Header.AGR_Name
                ,IV_Item.Item_ID as SKU
                ,IV_Item.Description
                ,IV_Item.Long_Description as CustomerDescription
                ,AGR_Detail.AGD_Desc
                ,AGR_Header.AGR_Date_End
                ,AGR_Detail.AGD_Date
                ,AGR_Detail.AGD_Qty
                ,AGR_Detail.AGD_Cancel
                ,AGR_Detail.AGD_Cost
                ,AGR_Detail.Extended_Cost_Amount
                ,AGR_Detail.Date_Entered_UTC
                ,AGR_Detail.Last_Update

            FROM AGR_Detail AGR_Detail
            left join AGR_Header on AGR_Detail.AGR_Header_RecID = AGR_Header.AGR_Header_RecID
            left join Company on AGR_Header.Company_RecID = Company.Company_RecID
            left join iv_item on AGR_Detail.IV_Item_RecID = IV_Item.IV_Item_RecID
            WHERE AGR_Detail.AGR_Header_RecID IN 
                (
                    SELECT agr_header_recid FROM AGR_Header
                    WHERE agr_type_recid in (19,45,60)
                    AND (agr_date_end >= GETDATE() or agr_date_end is NULL)
                )
            AND AGR_Detail.AGD_Cancel is NULL
            AND AGR_Header.AGR_Cancel_Flag = 0
            and Company.Company_Status_RecID != 21
            order by Company.Company_ID`,
    
    cwTime: `Select TOP 100 * from dbo_v_api_collection_time_entry where DATE_ENTERED_UTC >= '2021-03-01'`,
    cwCompany: `SELECT * FROM [cwwebapp_choice].[dbo].[Company] co where co.Company_Status_RecID = 25 or Company_RecID in (2, 16004, 22484)`,
    cwCompanyID: `SELECT 
        [Company_RecID]
        ,[Company_ID]
        ,[Company_Name]
    FROM [cwwebapp_choice].[dbo].[Company] co 
    where 
        co.Company_Status_RecID = 25 
        or Company_RecID in (2, 16004, 22484)`,
    cwOpenedMSTickets: `SELECT 
    [Company_RecID]
    ,[Company_ID]
    ,[SR_Service_RecID]
    ,[Date_Entered_UTC]
    ,[Company_Name]
    ,[Ticket_Owner_Name]
    ,[Summary]
    ,[Rec_Type]
    ,[Board_Name]
    ,[Status_Name]
    ,[Contact_Name]
    ,[Site_Name]
    ,[Email_Address]
    ,[SR_Type_Name]
    ,[SR_SubType_Name]
    ,[SR_SubTypeItem_Name]
    ,[SR_Team_Name]
    ,[SR_Urgency_Name]
    ,[SR_Source_Name]
    ,[Hours_Actual]
    ,[AGR_Name]
    ,[SR_Severity_Name]
    ,[SR_Impact_Name]
    ,[Date_Responded_UTC]
    ,[Updated_By]
    ,[Last_Update_UTC]
    ,[ResourceList]
    ,[SLA_Status_Text]
    ,[parentTicketId]
    ,[HasChild]
    ,[Ticket_Owner_ID]
    ,[Ticket_Owner_Last_Updated_UTC]
    ,[SR_SLA_RecID]
    ,[SLA_Name]
    ,[Contact_Full_Name]
FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
where

(Board_Name  LIKE '%Change Management%'	
OR	Board_Name  LIKE '%Automation%'		
OR	Board_Name  LIKE '%Citrix%'	
OR	Board_Name  LIKE '%Network%'		
OR	Board_Name  LIKE '%NOC%'		
OR	Board_Name  LIKE '%Patching%'		
OR	Board_Name  LIKE '%Security%'		
OR	Board_Name  LIKE '%Virtualization%')
and 
Date_Entered_UTC >= DATEADD(day, -14, GETDATE())
order by Summary`,
cwMsBilling: `SELECT bl.Billing_Log_RecID,co.Company_Name,co.Company_RecID, 
co.Company_ID,
agt.AGR_Type_Desc,
DATENAME(month, bl.Date_Invoice) as month,
year(bl.date_invoice) as year,
sum(bl.Invoice_Amount) - sum(bl.Sales_Tax_Amount)as Invoice_Amount, 
sum(bl.Sales_Tax_Amount) as Tax_Amount, 
sum(bl.Invoice_Amount) as Total
FROM [cwwebapp_choice].[dbo].[Billing_Log] bl
join company co on bl.company_recID = co.company_recid
join AGR_Header ah on bl.AGR_Header_RecID = ah.AGR_Header_RecID
join AGR_Type agt on ah.AGR_Type_RecID = agt.AGR_Type_RecID
where  
co.Company_Status_RecID = 25 
and 
YEAR(bl.Date_Invoice) >= year(getdate()) -3
and 
ah.AGR_Type_RecID in (select agr_type_recid   FROM [cwwebapp_choice].[dbo].[AGR_Type] where agr_type_desc like '%managed%')
group by bl.Billing_Log_RecID,co.company_name,co.Company_RecID,co.Company_ID, year(bl.date_invoice),DATENAME(month, bl.Date_Invoice),agt.AGR_Type_Desc
order by Company_Name,month`,
cwBilling: `SELECT bl.Billing_Log_RecID,co.Company_Name,co.Company_RecID, 
co.Company_ID,
agt.AGR_Type_Desc,
DATENAME(month, bl.Date_Invoice) as month,
year(bl.date_invoice) as year,
sum(bl.Invoice_Amount) as Invoice_Amount, 
sum(bl.Sales_Tax_Amount) as Tax_Amount, sum(bl.Invoice_Amount) - sum(bl.Sales_Tax_Amount) as Total,
AGR_Type_desc
FROM [cwwebapp_choice].[dbo].[Billing_Log] bl
join company co on bl.company_recID = co.company_recid
join AGR_Header ah on bl.AGR_Header_RecID = ah.AGR_Header_RecID
join AGR_Type agt on ah.AGR_Type_RecID = agt.AGR_Type_RecID
where  
YEAR(bl.Date_Invoice) >= year(getdate()) -3

group by bl.Billing_Log_RecID,co.company_name,co.Company_RecID,co.Company_ID, year(bl.date_invoice),DATENAME(month, bl.Date_Invoice),agt.AGR_Type_Desc
order by Company_Name,month`,
cwNewTicket: `Select * from (
    SELECT 
    'Follow-Up' as Type,
    [dbo].[v_rpt_Service].[SR_Service_RecID] AS 'Rec_ID', 
    [dbo].[v_rpt_Service].[company_name] AS 'Company Name', 
    [dbo].[v_rpt_Service].[Summary] AS 'Summary', 
    [dbo].[v_rpt_Service].[Board_Name] AS 'Board Name', 
    [dbo].[v_rpt_Service].[status_description] AS 'Status Description'FROM [dbo].[v_rpt_Service] WITH(NOLOCK) 
    INNER JOIN [dbo].[v_rpt_Company] WITH(NOLOCK)  ON [dbo].[v_rpt_Company].[Company_RecID]=[dbo].[v_rpt_Service].[company_recid]
    INNER JOIN [dbo].[v_rpt_ServiceStatus] WITH(NOLOCK)  ON [dbo].[v_rpt_ServiceStatus].[SR_Status_RecID]=[dbo].[v_rpt_Service].[sr_status_recid]
    INNER JOIN [dbo].[v_rpt_ServicePriority] WITH(NOLOCK)  ON [dbo].[v_rpt_ServicePriority].[SR_Urgency_RecID]=[dbo].[v_rpt_Service].[SR_Urgency_RecID]
    INNER JOIN [dbo].[SR_Service] WITH(NOLOCK)  ON [dbo].[SR_Service].[SR_Service_RecID]=[dbo].[v_rpt_Service].[SR_Service_RecID]
    WHERE  ([dbo].[v_rpt_Service].[Closed_Flag] = 0) AND (NOT([dbo].[SR_Service].[CustUpdate_Flag] IS NULL OR CAST([dbo].[SR_Service].[CustUpdate_Flag] AS nvarchar)='')) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 75)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 31)) AND (NOT([dbo].[v_rpt_Service].[SR_Board_RecID] = 12)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 10)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 26)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 11)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 85)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 89)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 87)) AND (NOT([dbo].[SR_Service].[SR_Board_RecID] = 76)) 
    union
    SELECT   
    'New' as Type,
    [dbo].[v_rpt_Service].[SR_Service_RecID] AS 'Rec_ID', 
    [dbo].[v_rpt_Service].[company_name] AS 'Company Name', 
    [dbo].[v_rpt_Service].[Summary] AS 'Summary', 
    [dbo].[v_rpt_Service].[Board_Name] AS 'Board Name', 
    [dbo].[v_rpt_Service].[status_description] AS 'Status Description'
    FROM [dbo].[v_rpt_Service] WITH(NOLOCK) 
    WHERE (([dbo].[v_rpt_Service].[Closed_Flag] = 0) 
    AND ([dbo].[v_rpt_Service].[status_description] = 'New Email') 
    AND 1 = 1 AND (NOT([dbo].[v_rpt_Service].[SR_Board_RecID] = 75)) 
    AND (NOT([dbo].[v_rpt_Service].[SR_Board_RecID] = 76)) 
    AND (NOT([dbo].[v_rpt_Service].[SR_Board_RecID] = 89))) OR (([dbo].[v_rpt_Service].[Summary] LIKE ISNULL('Alarm', '') + ISNULL('%','')) AND ([dbo].[v_rpt_Service].[Summary] LIKE ISNULL(ISNULL('%', '') + ISNULL('Critical',''), '') + ISNULL('%','')) 
    AND ([dbo].[v_rpt_Service].[status_description] = 'Unassigned')) OR (([dbo].[v_rpt_Service].[company_name] LIKE ISNULL('Soleo', '') + ISNULL('%','')) 
    AND ([dbo].[v_rpt_Service].[status_description] = 'Unassigned'))
    ) as t
    order by Rec_ID`,
cwCriticalTickets: `select * from v_ChoiceSolutions_SLACriticalTickets where ServiceBoard != 'Backup'`,
cwSLADaysOut: `SELECT 
case
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '24' AND '49' then 2
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '48' AND '73' then 3
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '48' AND '97' then 4
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '96' AND '121' then 5
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '121' AND '145' then 6
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '144' AND '169' then 7
when [dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '168' AND '193' then 8
end as 'Days Out',
[dbo].[v_ChoiceSolutions_SLANotViolated].[TicketNum] AS 'Ticket Num', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[ResolveBy] AS 'Resolve By', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] AS 'Hours Left', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[Company] AS 'Company', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[SummaryDescription] AS 'Summary Description', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[ServiceBoard] AS 'Service Board', 
[dbo].[v_ChoiceSolutions_SLANotViolated].[Status] AS 'Status', 
[dbo].[v_rpt_Service].[Ticket_Owner_First_Name] AS 'Ticket Owner First Name', 
[dbo].[v_rpt_Service].[Ticket_Owner_Last_Name] AS 'Ticket Owner Last Name'
FROM [dbo].[v_ChoiceSolutions_SLANotViolated] WITH(NOLOCK) 
INNER JOIN [dbo].[v_rpt_Service] WITH(NOLOCK)  ON [dbo].[v_rpt_Service].[TicketNbr]=[dbo].[v_ChoiceSolutions_SLANotViolated].[TicketNum]
WHERE  
([dbo].[v_ChoiceSolutions_SLANotViolated].[HoursLeft] BETWEEN '24' AND '193') 
AND ([dbo].[v_ChoiceSolutions_SLANotViolated].[Status] IN ('--> Escalation Recommended','Assigned','In Progress','Unassigned')) 
order by 'Days Out'`,

cwBilling2: `SELECT bl.Billing_Log_RecID,co.Company_Name,co.Company_RecID, 
co.Company_ID,
agt.AGR_Type_Desc,
bl.Date_Invoice as date,
sum(bl.Invoice_Amount) as Invoice_Amount, 
sum(bl.Sales_Tax_Amount) as Tax_Amount, 
sum(bl.Invoice_Amount) - sum(bl.Sales_Tax_Amount) as Total,
AGR_Type_desc
FROM [cwwebapp_choice].[dbo].[Billing_Log] bl
join company co on bl.company_recID = co.company_recid
join AGR_Header ah on bl.AGR_Header_RecID = ah.AGR_Header_RecID
join AGR_Type agt on ah.AGR_Type_RecID = agt.AGR_Type_RecID
where  
YEAR(bl.Date_Invoice) >= year(getdate()) -3

group by bl.Billing_Log_RecID,co.company_name,co.Company_RecID,co.Company_ID, bl.Date_Invoice,agt.AGR_Type_Desc
order by Company_Name,Date`,
cwMSBillingDetail: `SELECT bl.Billing_Log_RecID,co.Company_Name,co.Company_RecID, 
co.Company_ID,
agt.AGR_Type_Desc,
ah.AGR_Name,agd.[AGD_Desc],agd.[AGD_Qty],agd.[AGD_Amount],
DATENAME(month, bl.Date_Invoice) as month,
year(bl.date_invoice) as year,
bl.Invoice_Amount as Invoice_Amount, 
bl.Sales_Tax_Amount as Tax_Amount, bl.Invoice_Amount - bl.Sales_Tax_Amount as Total
FROM [cwwebapp_choice].[dbo].[Billing_Log] bl
join company co on bl.company_recID = co.company_recid
join AGR_Header ah on bl.AGR_Header_RecID = ah.AGR_Header_RecID
join AGR_Type agt on ah.AGR_Type_RecID = agt.AGR_Type_RecID
join [dbo].[AGR_Detail] agd on bl.agr_header_recid = agd.[AGR_Header_RecID]
where  
co.Company_Status_RecID = 25 
and 
YEAR(bl.Date_Invoice) >= year(getdate()) - 1
and 
ah.AGR_Type_RecID in (select agr_type_recid   FROM [cwwebapp_choice].[dbo].[AGR_Type] where agr_type_desc like '%managed%')
and agd.AGD_Amount != 0
order by Company_Name,month`,
cwMSLastMonthBillingDetail: `SELECT
bl.Billing_Log_RecID,
co.Company_Name,
co.Company_RecID, 
co.Company_ID,
agd.[AGD_Qty] as qty,
ivi.Item_ID as SKU,
agt.AGR_Type_Desc as Description,
ah.AGR_Name,agd.[AGD_Desc] as 'Customer Description',
agd.[AGD_Amount] as 'Unit Price',
agd.[AGD_Amount]*agd.[AGD_Qty]  as 'Ext Price',
agd.[AGD_Cost] as Cost,
agd.[AGD_Cost]*agd.[AGD_Qty] as 'Ext Cost',
(agd.[AGD_Amount]*agd.[AGD_Qty])-(agd.[AGD_Cost]*agd.[AGD_Qty]) as 'GP',
cast(agd.[AGD_Cost]/agd.[AGD_Amount]*100 as int) as 'GP Margin'

FROM [cwwebapp_choice].[dbo].[Billing_Log] bl
join company co on bl.company_recID = co.company_recid
join AGR_Header ah on bl.AGR_Header_RecID = ah.AGR_Header_RecID
join AGR_Type agt on ah.AGR_Type_RecID = agt.AGR_Type_RecID
join [dbo].[AGR_Detail] agd on bl.agr_header_recid = agd.[AGR_Header_RecID]
join [dbo].[IV_Item] ivi on ivi.IV_Item_RecID = agd.IV_Item_RecID
where  
co.Company_Status_RecID = 25 
and 
DATEPART(m, bl.Date_Invoice) = DATEPART(m, DATEADD(m, -1, getdate()))
AND DATEPART(yyyy, bl.Date_Invoice) = DATEPART(yyyy, DATEADD(m, -1, getdate()))
and 
ah.AGR_Type_RecID in (select agr_type_recid   FROM [cwwebapp_choice].[dbo].[AGR_Type] where agr_type_desc like '%managed%')
and agd.AGD_Amount != 0
and agd.AGD_Cancel is null
order by Company_Name`,
cwOpenTickets: `SELECT 
[Company_RecID]
,[Company_ID]
,[SR_Service_RecID]
,[Date_Entered_UTC]
,[Company_Name]
,[Ticket_Owner_Name]
,[Summary]
,[Rec_Type]
,[Board_Name]
,[Status_Name]
,[Contact_Name]
,[Site_Name]
,[Email_Address]
,[SR_Type_Name]
,[SR_SubType_Name]
,[SR_SubTypeItem_Name]
,[SR_Team_Name]
,[SR_Urgency_Name]
,[SR_Source_Name]
,[Hours_Actual]
,[AGR_Name]
,[SR_Severity_Name]
,[SR_Impact_Name]
,[Date_Responded_UTC]
,[Updated_By]
,[Last_Update_UTC]
,[ResourceList]
,[SLA_Status_Text]
,[parentTicketId]
,[HasChild]
,[Ticket_Owner_ID]
,[Ticket_Owner_Last_Updated_UTC]

,[SR_SLA_RecID]
,[SLA_Name]
,[Contact_Full_Name]
FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
where

(	Board_Name  LIKE '%Automation%'		
OR	Board_Name  LIKE '%Citrix%'	
OR	Board_Name  LIKE '%Network%'		
OR	Board_Name  LIKE '%NOC%'		
OR	Board_Name  LIKE '%Patching%'		
OR	Board_Name  LIKE '%Security%'		
OR	Board_Name  LIKE '%Virtualization%')

and
Status_Name not in ('cancelled', 'closed', 'Completed')
and 
parentTicketId is null
order by Date_Entered_UTC`,
cwSocTime: `
SELECT TOP (1000) [Company_RecID]
      ,[Time_RecID]
      ,[Member_ID]
      ,[Time_Start]
      ,[Time_End]
      ,[Hourly_Rate]
      ,[Hours_Bill]
      ,[Hours_Actual]
      ,[SR_Service_RecID]
      ,[Notes]
      ,[Notes_Markdown]

  FROM [cwwebapp_choice].[dbo].[Time_Entry]

    where SR_Service_RecID in (1401762,1400496) 

  order by Date_start desc
`,
cwTicketLastNote: `
With 
OpenTickets_CTE (ticketID,Company_RecID,Company_Name,Board_Name,Date_Opened)
as
(
Select sr_service_recid as ticketID
,Company_RecID
,Company_Name
,Board_Name
,Date_Entered_UTC as Date_Opened
FROM [cwwebapp_choice].[dbo].[v_api_collection_service_ticket]
where

	(	Board_Name  LIKE '%Automation%'		
	OR	Board_Name  LIKE '%Citrix%'	
	OR	Board_Name  LIKE '%Network%'		
	OR	Board_Name  LIKE '%NOC%'		
	OR	Board_Name  LIKE '%Patching%'		
	OR	Board_Name  LIKE '%Security%'		
	OR	Board_Name  LIKE '%Virtualization%'
	)
	and Status_Name not in ('cancelled', 'closed')

	
	
)

select Company_RecID,Company_Name, Board_Name, Date_Opened, seq, ticketId, noteId, n.Text_Markdown as note from
 (select  id as noteId
	,o.Company_Name
	,o.Company_RecID
	,o.Board_Name
	,o.Date_Opened
	,row_number() over(partition by sr_service_recid order by ticketId desc, id desc)  as seq
	,SR_Service_RecID as ticketId
	
   FROM OpenTickets_CTE o
	left join [cwwebapp_choice].[dbo].[v_api_collection_service_ticket_note] notes on o.ticketID = notes.SR_Service_RecID
   where notes.SR_Service_RecID is not NULL) t
   join [cwwebapp_choice].[dbo].[v_api_collection_service_ticket_note] n on t.noteId = n.ID
where seq in (1,2)`
}

module.exports = {
    cwMConfig,
    queries
}
